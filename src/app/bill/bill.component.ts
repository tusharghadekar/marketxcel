import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
import { FormControl, FormGroup } from '@angular/forms';
// import { FormControl, FormGroup } from '@angular/forms';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})
export class BillComponent implements OnInit {


  constructor(private _router: Router, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService, private user: UserService) { }

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public beforeUpdateUserObj;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  UserdData = [];
  UserDataCount: any = [];
  catname: any;
  id: any;
  selectedId: any =[];
  getUserData() {
    debugger
    this.spinnerService.show();
    let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  categoryData = [];
  
  getcategory(id) {
//debugger
    for(let i=0; i<this.categoryData.length; i++){
      if(this.categoryData[i].id === id){
        this.catname = this.categoryData[i].name;
        return this.catname;
        //console.log("catname: " + this.catname[i]);
      }
    }
  }
  getCategoryData() {
    debugger
    this.spinnerService.show();
    let localUrl = "category/";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  userpageNo: number = 1;
  pageChange(pageNo: any) {
    debugger
    this.selectedId;
    this.userpageNo = pageNo;
    console.log("pageNo: " + this.userpageNo);

    this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  ngOnInit() {
    this.getUserData();
    this.getCategoryData();
    this.selectedId={};
  }

}
