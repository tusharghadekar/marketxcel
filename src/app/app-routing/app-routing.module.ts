
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './../admin-dashboard/admin-dashboard.component';
import { LoginFormComponent } from './../login-form/login-form.component';
import { MasterColorComponent } from './../master-color/master-color.component';
import { CategoriesComponent } from './../categories/categories.component';
import { MasterStoreComponent } from './../master-store/master-store.component';
import { MasterProductsComponent } from './../master-products/master-products.component';
import { UsersComponent } from './../users/users/users.component';
import {InventoryManagementComponent} from './../inventory-management/inventory-management.component';
import { BillComponent } from './../bill/bill.component';
import { ShopReportComponent } from './../shop-report/shop-report.component';
import { MasterBrandComponent } from './../master-brand/master-brand.component';
import { ShopReportDetailsComponent } from './../shop-report-details/shop-report-details.component';
import { OffersComponent } from './../offers/offers.component';

//import { AuthguardGuard } from './../authguard.guard';
import { UserService } from './../user.service'
import { AuthguardGuard } from '../authguard.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'login-form', pathMatch: 'full' },
     
     { path: 'login-form', component: LoginFormComponent },
     { path: 'master-color', component: MasterColorComponent},
     { path: 'dashboard', component: AdminDashboardComponent},
     { path: 'categories', component: CategoriesComponent},
     { path: 'store/:id', component: MasterStoreComponent},
     { path:'brand/:id' , component:MasterBrandComponent},
     { path: 'products/:id', component: MasterProductsComponent},
     { path: 'users', component: UsersComponent},
     {path: 'inventory/:id', component:InventoryManagementComponent},
     {path: 'bill', component:BillComponent},
     {path: 'shopreport/:id', component:ShopReportComponent},
     {path: 'shopreportdetails/:id', component:ShopReportDetailsComponent},
     {path: 'offers', component:OffersComponent}
    ],{ useHash: true })
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
