import { Injectable } from '@angular/core';


@Injectable()
export class sharedService {
    _shop_id:number;
    _subassembly_id:number;
    _Chryslercondition_id:number;
    _VehicleProjects_id:number;
    _opsVehicleAudits_id:number;
    _opsDetailssId:number;
    _filterCountryId:any;
    _filterVechicleId:any;
    _filterAuditorId:any;
    _filterApprovedId:any;
    _editVechicleNameId:any;
    _editbodycolourId:any;
    _editroofcolourId:any;
    _editversionId:any;
    _editcountryId:any;
    _editengineId:any;
    _editauditorId:any;
    _transmissionId:any;
    _VIN:any;
    _subcatId:number;
    _questionId:number;



    public setSubcat(_subcatId:number){
        this._subcatId = _subcatId;
    }
    public getSubcat(){
       return this._subcatId;
    }
    public setVIN(_VIN:any){
        this._VIN = _VIN;
    }
    public getVIN(){
        return this._VIN;
    }
    public setEditVechicleName(editVechicleNameId:any){
        this._editVechicleNameId = editVechicleNameId;
    }
    public getEditVechicleName(){
        return this._editVechicleNameId;
    }
    public setEditBodyColourid(editbodycolourId:any){
        this._editbodycolourId = editbodycolourId;
    }
    public getEditBodyColourid(){
        return this._editbodycolourId;
    }
    public setEditRoofColourid(editroofcolourId:any){
        this._editroofcolourId = editroofcolourId;
    }
    public getEditRoofColourid(){
        return this._editroofcolourId;
    }
    public setEditVersionid(editversionId:any){
        this._editversionId = editversionId;
    }
    public getEditVersionid(){
        return this._editversionId;
    }
    public setEditCountryid(editcountryId:any){
        this._editcountryId = editcountryId;
    }
    public getEditCountryid(){
        return this._editcountryId;
    }
    public setEditEngineid(editengineId:any){
        this._editengineId = editengineId;
    }
    public getEditEngineid(){
        return this._editengineId;
    }
    public setEditAuditorid(editauditorId:any){
        this._editauditorId = editauditorId;
    }
    public getEditAuditorid(){
        return this._editauditorId;
    }

    public setEdittransmissionId(transmissionId:any){
        this._transmissionId = transmissionId;
    }
    public getEdittransmissionId(){
        return this._transmissionId;
    }

    

    



public setCurrentShopsid(shop_id:number){
    this._shop_id = shop_id;
}
public getCurrentShopsid(){
    return this._shop_id;
}

public setCurrentSubassemblyid(subassembly_id:number){
    this._subassembly_id = subassembly_id;
}
public getCurrentSubassemblyid(){
    return this._subassembly_id;
}

public setCurrentChryslerconditionid(Chryslercondition_id:number){
    this._Chryslercondition_id = Chryslercondition_id;
}
public getCurrentChryslerconditionid(){
    return this._Chryslercondition_id;
}

public setCurrentVehicleProjectsid(VehicleProjects_id:number){
    this._VehicleProjects_id = VehicleProjects_id;
}
public getCurrentVehicleProjectsid(){
    return this._VehicleProjects_id;
}

public setCurrentopsVehicleAuditsid(opsVehicleAudits_id:number){
    this._opsVehicleAudits_id = opsVehicleAudits_id;
}
public getCurrentopsVehicleAuditsid(){
    return this._opsVehicleAudits_id;
}

public setCurrentopsVehicleDetailsid(opsDetailssId:number){
    this._opsDetailssId = opsDetailssId;
}
public getCurrentopsVehicleDetailsid(){
    return this._opsDetailssId;
}

public setFilterForCountryid(filterCountryId:any){
    this._filterCountryId = filterCountryId;
}
public getFilterForCountryid(){
    return this._filterCountryId;
}

public setFilterFoVechicleid(filterVechicleId:any){
    this._filterVechicleId = filterVechicleId;
}
public getFilterForVechicleid(){
    return this._filterVechicleId;
}

public setFilterForAuditorid(filterAuditorId:any){
    this._filterAuditorId = filterAuditorId;
}
public getFilterForAuditorid(){
    return this._filterAuditorId;
}

public setFilterForApprovedid(filterApprovedId:any){
    this._filterApprovedId = filterApprovedId;
}
public getFilterForApprovedid(){
    return this._filterApprovedId;
}



} //sharedService Close