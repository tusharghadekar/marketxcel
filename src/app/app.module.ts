
import './../polyfills';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
//import { DataTablesModule } from 'angular-datatables';


import { BrowserModule } from '@angular/platform-browser';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HeaderComponent } from './header/header.component';
import { LeftSideComponent } from './left-side-bar/left-side-bar.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { AppComponent } from './app.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//extra component
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { NgDatepickerModule } from 'ng2-datepicker';
import { ChartsModule } from 'ng2-charts';
import {NgxPaginationModule} from 'ngx-pagination';
//import {WebStorageModule, LocalStorageService} from "angular-localstorage";

import {CdkTableModule} from '@angular/cdk/table';
import { LoginFormComponent } from './login-form/login-form.component';

import { UserService } from './user.service';
import { sharedService } from './services/masters/sharedService.service';
import { DateService } from './services/date/date.service';
import { auditorsprofileService } from './services/masters/auditorsprofile.service';
import { vehiclemanagerservices } from './services/vehiclemanager/vehiclemanager.service';
import { ReportService }from './services/report/report.service';

import { AuthguardGuard } from './authguard.guard';
import { MasterColorComponent } from './master-color/master-color.component';
import { AuthService } from './interceptors/auth.service';

import { Ng2OrderModule } from 'ng2-order-pipe';
import { UsersComponent } from './users/users/users.component';
import { CategoriesComponent } from './categories/categories.component';
import { MasterStoreComponent } from './master-store/master-store.component';
import { MasterProductsComponent } from './master-products/master-products.component';
import { ModalDialogModule } from 'ngx-modal-dialog';
import {ModalModule} from "ngx-modal";
import { InventoryManagementComponent } from './inventory-management/inventory-management.component';
// import { NgxEchartsModule } from 'ngx-echarts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BillComponent } from './bill/bill.component';
import { ShopReportComponent } from './shop-report/shop-report.component';
import { MasterBrandComponent } from './master-brand/master-brand.component';
import { ShopReportDetailsComponent } from './shop-report-details/shop-report-details.component';
import { OffersComponent } from './offers/offers.component';


@NgModule({
  imports: [
       BrowserModule,
	AppRoutingModule,
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    FormsModule,
    HttpModule,
	HttpClientModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    MyDateRangePickerModule,
    NgDatepickerModule,
    ChartsModule,
    NgxPaginationModule,
    AmazingTimePickerModule,
    Ng2OrderModule,
    ModalDialogModule,
    ModalModule,
    // NgxEchartsModule,
    Ng2SearchPipeModule
    //DataTablesModule
  ],
 declarations: [
    AdminDashboardComponent,
    AppComponent,
    HeaderComponent,
    LeftSideComponent,
    ContentComponent,
    FooterComponent,
    LoginFormComponent,
    MasterColorComponent,
    UsersComponent,
    CategoriesComponent,
    MasterStoreComponent,
    MasterProductsComponent,
    InventoryManagementComponent,
    BillComponent,
    ShopReportComponent,
    MasterBrandComponent,
    ShopReportDetailsComponent,
    OffersComponent,
    
   
  ],
  entryComponents: [AppComponent],
  bootstrap: [AppComponent],
  providers: [
    UserService,
    AuthguardGuard,
    sharedService,
    DateService,
    auditorsprofileService,
    vehiclemanagerservices,
    ReportService,
    AuthService
]
})
export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);



/**  Copyright 2017 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
