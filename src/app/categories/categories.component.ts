import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  public viewCat:string;
  server_url : string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
  public beforeUpdateCatObj;
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };
  constructor(private _router: Router, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService, private user: UserService) { }

  ShowCatTable(){
    this.viewCat = 'addCat';
  }
  redirect(id) {
    this._router.navigate(['brand/'+id]);
  }

  onSearchChange(searchValue : string ) {
    debugger
    this.spinnerService.show();
    let localUrl = "category/?name="+searchValue;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    console.log(searchValue);
  
  }

  public addCat(e): void{
    e.preventDefault();
    debugger;
    let name =  e.target.elements[0].value;
    let desc =  e.target.elements[1].value;
    const addCatObject = {name : name, description: desc}

let localUrl = "category/";
let url = this.server_url+localUrl;

this.http.post(url, JSON.stringify(addCatObject), this.httpOptions)
.subscribe(
  (res) => {
    if(res) {
     alert("You added Category successfully..");
     this.viewCat= '';
     this.getCategoryData();
        }else{
          alert("Not Added");
        }
  },
  err => {alert( "Error occoured");
  this.viewCat= '';
}
  );
}

updateCat(beforeUpdateCatObj) {
debugger
  this.beforeUpdateCatObj = { 
    "name": beforeUpdateCatObj.name,
    "description": beforeUpdateCatObj.description,
    "is_active":beforeUpdateCatObj.is_active
 };
 let localUrl = "category/"+beforeUpdateCatObj.id+"/";
 let url = this.server_url+localUrl;

 this.http.patch(url, JSON.stringify(this.beforeUpdateCatObj), this.httpOptions).subscribe(
   (res) => {
     if(res)
     this.beforeUpdateCatObj = {
      "id": res['id'],
      "name": res['name'],
      "description": res['description'],
      "is_active": res['is_active']
     };
     alert("Successfully Updated");
    this.viewCat =''; 
    this.getCategoryData();      
   }, err =>{alert("You made no change");}
       );
    
     
} // Update Auditor functionality close

// Editing Auditor function 
editCat(categoryData){
  
         if(confirm("You want to edit Category ?")){
        debugger;
         this.viewCat = 'editCat';
         let localUrl = "category/"+categoryData.id+"/";
         let url = this.server_url+localUrl;
         const checkcatObject = { id : categoryData.id }

 this.http.patch(url,JSON.stringify(checkcatObject), this.httpOptions).subscribe(
 (res) => {
   if(res)
   this.beforeUpdateCatObj = {
     "id": res['id'], 
    "name": res['name'],
    "description": res['description'],
    "is_active": res['is_active']
    
   };

       }, err =>{alert("You made no change");}
     );
   }else{}
  
 }

 deleteCat(categoryData){
  if(confirm("You want to Delete Category?")){
 debugger;
  let localUrl = "category/"+categoryData.id+"/";
  let url = this.server_url+localUrl;

this.http.delete(url,this.httpOptions).subscribe(
(res) => {
alert("You Deleted Category");
this.viewCat="";
this.getCategoryData();
}, err =>{alert("You made no change");}
);
}else{alert("error occured");}

}

categoryData = [];
catDataCount;
  getCategoryData() {
    debugger
    this.spinnerService.show();
    let localUrl = "category";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  catid
  filterdata(form: any): void {
    debugger
    this.spinnerService.show();

    if (form.value.listSearch != null  && form.value.listSearch.length > 0) {
      this.catid = form.value.listSearch;
    }
    
    let localUrl = "category/"+this.catid;
    let url = this.server_url + localUrl;
    
    this.http.get(url)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  onIsActiveChange(id:number, isActive:string){
    //
    if(confirm("Are you sure ?")){
      if(isActive == 'true')
      {
        isActive = 'false';
      }else if(isActive == 'false'){
        isActive = 'true';
      }
      let localUrl = "category/"+id;
      let url = this.server_url+localUrl;
  
      
      const deactiveVehicleSalesObject = { is_active : isActive }
      let jsonVehicleSalesObjectBody = JSON.stringify(deactiveVehicleSalesObject);
  
  this.http.patch(url, jsonVehicleSalesObjectBody, this.httpOptions).subscribe(
  (res) => {
    if(res)
  //this.categoryData = res['data'];
  alert("You have changed staus  successfully..");
  //this.viewopsVehicleSalesData = '';
  this.getCategoryData();
  });
  }else{alert("You made no change"); 
  // this.getopsVehicleAuditsMasterData(this.VehicleAuditspageNo); this.viewopsVehicleAudits = '';
  }
  }

  catpageNo: number = 1;
  pageChange(pageNo: any) {
    debugger
    this.catpageNo = pageNo;
    console.log("pageNo: " + this.catpageNo);

    this.spinnerService.show();
    let localUrl = "category/?page="+this.catpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    
    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
  }

  ngOnInit() {
    this.getCategoryData();
  }

}
