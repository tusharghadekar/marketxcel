import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterStoreComponent } from './master-store.component';

describe('MasterStoreComponent', () => {
  let component: MasterStoreComponent;
  let fixture: ComponentFixture<MasterStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
