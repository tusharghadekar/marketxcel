import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { environment } from '../../environments/environment';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  public href: string = "";
  public loginObject: any = {username: '', password:''};
  public loginObjjson= {};
  public jsonloginbody :any;
  public userName: string = "";
  public userRoleid: number;
  constructor(private spinnerService: Ng4LoadingSpinnerService,private router:Router, private user:UserService, private http:HttpClient) { }
  server_url : string = environment.devServer_url;
  
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  ngOnInit() {
    $('body').removeClass('page-header-fixed');
    this.href = this.router.url;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.clear();
  }
  public loginUser(e): void {
    // this.router.navigate(['./dashboard']);
    this.spinnerService.show();
  	e.preventDefault();
    console.log(e);
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;
    localStorage.setItem('currentUser', username);
    debugger
    // if(username  ==='admin' && password === '123456'){
    //   //console.log('--inside logiin ---',this.login);
    //   this.user.setUserLoggedIn();
    //      this.user.setUserLoggedInName(this.userName);
    //   this.router.navigate(['/dashboard']);
    //   localStorage.setItem('currentUser', JSON.stringify(this.userName));
    //   //this.loginData =this.login.psw;
    //   this.spinnerService.hide();
    // }
    // else{
    //   this.router.navigate(['']);
    //  alert("invalid Credentials");
    //  this.spinnerService.hide();
    // }
  
    
     const addloginObject = {username: username, password: password}
    
let localUrl = "login/";
let url = this.server_url+localUrl;
debugger;

this.http.post(url, JSON.stringify(addloginObject), this.httpOptions)
.subscribe(
    (res:Response) => {
      this.spinnerService.hide();
          // this.userName = res['data']['auditor_name'];
          // this.userRoleid = res['data']['user_role_id'];
          // let currentUserid = res['data']['auditor_id'];
          let token = res['token'];
          this.user.setUserLoggedIn();
          this.user.setUserLoggedInName(this.userName);
          // localStorage.setItem('currentUser', JSON.stringify(this.userName));
          localStorage.setItem('userRoleid', JSON.stringify(this.userRoleid));
          localStorage.setItem('token', token);
          
          // this.router.navigate(['dashboard']);
          this.router.navigate(['./dashboard']);
          //console.log("userNameLogin"+this.userName);
          this.spinnerService.hide();
    },
    err => {
      this.spinnerService.hide();
      
     
      
        alert("Invalid username or password");
      
  }
    );

  } // login user close

public saveUserProfile(): void{
// this.profileService.saveProfile().subscribe(res => this.profileObject = res);
}
// redirect(){
//   this.router.navigate(['./dashboard']);
// }

}
