import { Injectable } from '@angular/core';


@Injectable()
export class ReportService {
    _ProjectId:number;
    _exportFilterBody:any;
    _ShopId:number;
    
    _ProjectIdtopfive:number;
    _VehicleIdtopfive:number;

    public setProjectId(ProjectId:number){
        this._ProjectId = ProjectId;
    }
    public getProjectId(){
        return this._ProjectId;
    }

    public setShopId(ShopId:number){
        this._ShopId = ShopId;
    }
    public getShopId(){
        return this._ShopId;
    }

    setFilterData(exportFilterBody){
        this._exportFilterBody = exportFilterBody;
    }

    getFilterData(){
     return this._exportFilterBody;
    }

    public settopfiveProjectId(ProjectIdtopfive:number){
        this._ProjectIdtopfive = ProjectIdtopfive;
    }
    public gettopfiveProjectId(){
        return this._ProjectIdtopfive;
    }

    public settopfiveVehicleId(VehicleIdtopfive:number){
        this._VehicleIdtopfive = VehicleIdtopfive;
    }
    public gettopfiveVehicleId(){
        return this._VehicleIdtopfive;
    }
    
    

} //sharedService Close