import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-left-side',
  templateUrl: './left-side-bar.component.html',
  styleUrls: ['./left-side-bar.component.css']
})
export class LeftSideComponent implements OnInit {

  constructor( private _router: Router,private _activatedRoute: ActivatedRoute) {}
  ngOnInit() {
  }
  redirect() {
    this._router.navigate(['.audit/vehicleaudit']);
  }
}
