import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { DateService } from '../../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../../user.service';
import { AuthService } from '../../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
import { FormControl, FormGroup } from '@angular/forms';
import { auditorsprofileService } from '../../services/masters/auditorsprofile.service';
// import { FormControl, FormGroup } from '@angular/forms';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public viewUser: string;
  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public beforeUpdateUserObj;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd.mm.yyyy',
    sunHighlight: true,
    height: '40px',
    width: '100%',
    inline: false,
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    
  };

  constructor(private _router: Router, private _auditorsprofileservice: auditorsprofileService, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService, private user: UserService) { }
  //let token = JSON.parse(localStorage.getItem('token'));
  ShowUserTable() {
    this.viewUser = 'addUser';
  }
  checkValueMemberobject='0';

  checkValueMember(event: any){
    console.log(event);
    this.checkValueMemberobject= event;
 }

  public addUser(form:any): void {
    //e.preventDefault();
    debugger;

    let username =form.value.username;
    let email = form.value.email;
    let address = form.value.address;
    let city = form.value.city;
    let Mobile = form.value.Mobile;
    let shopname = form.value.shopname;
    let gstno = form.value.gstno;
    //let category = e.target.elements[6].value;
    let lat = form.value.lat;
    let long = form.value.long;
    let master_user_pass = form.value.password;
    let master_user_confirmPass = form.value.cpassword;

    let formData: FormData = new FormData();
    // let file: File = this.filedata;
    // formData.append("image", file);
    let httpOptions= {
      headers: new HttpHeaders({
        //'content-type':'multipart/form-data',
        'Authorization': 'JWT ' + this.tokens
      })
    };

    if (((master_user_pass.length) >= 7) && ((master_user_confirmPass.length) >= 7)) {
      if ((master_user_pass == master_user_confirmPass)) {

        // const addUserObject = {
        //   username: username,
        //   email: email,
        //   address: address,
        //   mobile_no: Mobile,
        //   shop_name: shopname,
        //   gst_no: gstno,
        //   category: category,
        //   password: master_user_pass,
        // }
        formData.append('username', username);
        formData.append('code',username);
        formData.append('is_panel_member',this.checkValueMemberobject);
        formData.append('email',email);
        formData.append('address',address);
        formData.append('city',city);
        formData.append('mobile_no',Mobile);
        formData.append('shop_name',shopname);
        formData.append('gst_no',gstno);
        //formData.append('category',category);
        formData.append('password',master_user_pass);
        // formData.append('image',file.name);
        let catarray = [];
      
        for (let i = 0; i < form.value.itemNamecat.length; i++) {
          catarray.push(form.value.itemNamecat[i].id);
          formData.append('category', catarray[i]);
        }
        

        debugger;
        let localUrl = "user/";
        let url = this.server_url + localUrl;
        this.http.post(url, formData, httpOptions)
          .subscribe(
            (res) => {
              if (res) {
                alert("You added new user successfully..");
                this.viewUser = '';
                this.getUserData();
              } else {
                alert("user NOT Added");
              }
            },
            err => {
              alert("Error occoured");
              // this.viewAuditor ='';
            }
          );
      } else { alert("Confirm Password and Password not match !"); }
    } else {
      alert("Please enter password minimum 7 character");
    }
  }
  uploadForm = new FormGroup({
    //file1: new FormControl()
  });
  filedata: File;

  onFileChange(e) {
    debugger
    this.filedata = e.target.files[0];
    //this.filedata.getAsBinary();
    // let formData: FormData = new FormData();
    // let file: File = this.filedata;
    // formData.append("image", file);
    // //formData.append("Key","targetLineFile");
    // //let headers = new Headers();
    // //headers.append('Content-Type', 'multipart/form-data');
    // //headers.append('Authorization', 'JWT ' + this.tokens);
    // let httpOptions= {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'multipart/form-data',
    //     'Authorization': 'JWT ' + this.tokens
    //   })
    // };
    // let localUrl = "user/";
    // let url = this.server_url + localUrl;
    // this.http.post(url, formData,httpOptions)
    //   .subscribe((res) => {
    //     console.log("res" + res);
    //     if (res) {
    //       alert("Your Image uploaded successfully..");
    //       this.spinnerService.hide();
    //       this.viewUser = '';
    //       this.getUserData();
    //     } else {
    //       alert("File NOT Uploaded");
    //       this.spinnerService.hide();
    //     }
    //   });
  }


  updateUser(beforeUpdateUserObj) {
    debugger
    // if(beforeUpdateUserObj.category != undefined){
      var category = [];
      for (let i = 0; i < beforeUpdateUserObj.category.length; i++) {
        if(beforeUpdateUserObj.category[i].id == undefined){
          category.push(beforeUpdateUserObj.category[i]);
        }else{
          category.push(beforeUpdateUserObj.category[i].id);
        }
        
        //this.beforeUpdateUserObj.append('category[]', catarrayy[i]);
        //this.beforeUpdateUserObj.push(category);
      }
    // }else{
    //   category.push(this.beforeUpdateUserObj.category);
    // }
  
    this.beforeUpdateUserObj = {
      "username": beforeUpdateUserObj.username,
      "code": beforeUpdateUserObj.code,
      "is_panel_member": this.checkValueMemberobject,
      "email": beforeUpdateUserObj.email,
      "address": beforeUpdateUserObj.address,
      "city": beforeUpdateUserObj.city,
      "mobile_no": beforeUpdateUserObj.mobile_no,
      "shop_name": beforeUpdateUserObj.shop_name,
      "gst_no": beforeUpdateUserObj.gst_no,
      "category": category
    };
    

    let localUrl = "user/" + beforeUpdateUserObj.id + "/";
    let url = this.server_url + localUrl;

    this.http.patch(url, JSON.stringify(this.beforeUpdateUserObj), this.httpOptions).subscribe(
      (res) => {
        if (res) {
          this.beforeUpdateUserObj = {
            "username": res['username'],
            "code": res['code'],
            "email": res['email'],
            "is_panel_member": res['is_panel_member'],
            "address": res['address'],
            "city": res['city'],
            "mobile_no": res['mobile_no'],
            "shop_name": res['shop_name'],
            "gst_no": res['gst_no'],
            "category": res['category']
          };
          alert("You udated successfully..");
          this.viewUser = '';
          this.getUserData();
        } else { alert("error Ocuured") }
      },
    );


  } // Update Auditor functionality close

  // Editing Auditor function 
  editUser(id) {

    if (confirm("You want to edit User ?")) {
      debugger;
      this.viewUser = 'editUser';
      let localUrl = "user/" +id + "/";
      let url = this.server_url + localUrl;
      const checkUserObject = { id: id }

      this.http.get(url, this.httpOptions).subscribe(
        (res) => {
          if (res){
            this.beforeUpdateUserObj = {
              "id": res['id'],
              "code": res['code'],
              "username": res['username'],
              "is_panel_member": res['is_panel_member'],
              "email": res['email'],
              "address": res['address'],
              "city": res['city'],
              "mobile_no": res['mobile_no'],
              "shop_name": res['shop_name'],
              "gst_no": res['gst_no'],
              "category": res['category']
            };
          }
        }, err => { alert("You made no change"); }
      );
    } else { }

  }

  DeleteUser(UserdDataa) {
    if (confirm("You want to Delete  User ?")) {
      debugger;
      //this.viewUser = 'editUser';
      let localUrl = "user/" + UserdDataa.id + "/";
      let url = this.server_url + localUrl;
      //const checkUserObject = { id : UserdDataa.id }

      this.http.delete(url, this.httpOptions).subscribe(
        (res) => {
          alert("You Deleted User");
          this.viewUser = "";
          this.getUserData();
        }, err => { alert("You made no change"); }
      );
    } else { alert("error occured"); }

  }

  changePassword(id: number, name: string) {

    if (confirm("You want to change Password ?")) {
      this.viewUser = 'updatePassword';
      this._auditorsprofileservice.setcurrentAuditor(id);
    } else { alert("Password not changed"); }
  } //changePassword close

  updatePassword(password: any) {
// debugger
    let localUrl = "user/" + this._auditorsprofileservice.getcurrentAuditor() + "/";
    let url = this.server_url + localUrl;
    const updatePassword = { password: password }
    let jsonuserPasswordBody = JSON.stringify(updatePassword);
    debugger;
    if (password.length > 5) {
      this.http.patch(url, jsonuserPasswordBody, this.httpOptions).subscribe(
        (res) => {
          if (res)
            alert("You have changed Password  successfully..");
          this.viewUser = '';
          this.getUserData();
        });
    } else { alert("Please enter password minimum 6 character"); this.viewUser = ''; this.getUserData(); }
  }

  changeImage(id: number, name: string) {

    if (confirm("You want to change Image ?")) {
      this.viewUser = 'updateImage';
      this._auditorsprofileservice.setcurrentAuditor(id);
    } else { alert("Image not changed"); }
  } //changePassword close

  updateImage(image: any) {

    let formData: FormData = new FormData();
    let file: File = this.filedata;
    formData.append("image", file);
    let httpOptions= {
      headers: new HttpHeaders({
        //'content-type':'multipart/form-data',
        'Authorization': 'JWT ' + this.tokens
      })
    };

    formData.append('image',file.name);

    let localUrl = "user/" + this._auditorsprofileservice.getcurrentAuditor() + "/";
    let url = this.server_url + localUrl;
    
    debugger;
      this.http.patch(url, formData, httpOptions).subscribe(
        (res) => {
          if (res){
            alert("You have changed Image  successfully..");
          this.viewUser = '';
          this.getUserData();}
          else { alert("error Occured"); this.viewUser = ''; this.getUserData(); }
        });
    
  }
  filterbodyPagination;
  userpageNo: number = 1;
  pageChange(pageNo: any) {
    this.catName=[];
    debugger
    this.userpageNo = pageNo;
    console.log("pageNo: " + this.userpageNo);

    this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo+"&";
if(this.filterbodyPagination){
    if (this.filterbodyPagination.state) {
      localUrl = localUrl + "state=" +this.filterbodyPagination.state + "&"
      }
      if (this.filterbodyPagination.city) {
      localUrl = localUrl + "city=" +this.filterbodyPagination.city + "&"
      }
      if (this.filterbodyPagination.fromDate && this.filterbodyPagination.toDate) {
      localUrl = localUrl +"from="+this.filterbodyPagination.fromDate+"&"+"to="+this.filterbodyPagination.toDate;
      }
      if(this.filterbodyPagination.checkValueRegistered == true){
        localUrl = localUrl +"is_logged_in=1";
        }
    }

    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          for(var i = 0;i<this.UserdData.length;i++){
            let obj = this.UserdData[i];
            this.UserdData[i]['categories'] = [];
            for(let j=0; j<obj.category.length; j++ ){
              let catId = obj.category[j];
              let catName = this.getcategory(catId);
              if (this.UserdData[i]['categories']){
              this.UserdData[i]['categories'].push({
                'catId':catId,
                'catName':catName
              });
            }else{
              this.UserdData[i]['categories'] = [{}];
              this.spinnerService.hide();
            }
            this.spinnerService.hide();
            }
            this.spinnerService.hide();
          }         
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  UserdData = [];
  UserDataCount: any = [];
  catname: any;
  id: any;
  catName = [];


  // getUserData() {
  //   debugger
  //   this.catName=[];
  //   this.spinnerService.show();
  //   let localUrl = "user/";
  //   let url = this.server_url + localUrl;
  //   //this.catName=[];
  //   this.http.get(url, this.httpOptions)
  //     .subscribe(
  //       (res: Response) => {
          
  //         this.UserdData = res['results'];
  //         this.UserDataCount = res['count'];
  //         this.catName=[];
  //         this.UserdData.forEach(obj => {
  //           this.catName=[];
  //           for(let i=0; i<obj.category.length; i++ ){
  //             //this.catName=[];
  //           let localUrl = "category/"+obj.category[i];
  //           // this.catName=[];
  //           let url = this.server_url + localUrl;
            
  //           this.http.get(url,this.httpOptions)
  //             .subscribe(
  //               (res: Response) => {
  //                 //this.catName=[];
  //                 var xyz=res;
  //                // this.catName  = xyz["name"];
  //                 this.catName.push(xyz["name"])
  //                 // obj.catname= xyz["name"];
  //                 //this.catName=[];
  //                 this.spinnerService.hide();
  //               },
                
               
  //               err => { alert("Error occoured"); this.spinnerService.hide(); }
  //             );
  //           } 
  //           //this.catName=[];
  //       });
  //         this.spinnerService.hide();

  //       },
  //       err => { alert("Error occoured"); this.spinnerService.hide(); }
  //     );
  // }

  getUserData() {
    // debugger
    // this.catName=[];
    //this.getAllCategoryData(this.userpageNo);
    this.spinnerService.show();
    let localUrl = "user/";
    let url = this.server_url + localUrl;
    //this.catName=[];
    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          
          this.UserDataCount = res['count'];
          for(var i = 0;i<this.UserdData.length;i++){
            let obj = this.UserdData[i];
            this.UserdData[i]['categories'] = [];
            for(let j=0; j<obj.category.length; j++ ){
              let catId = obj.category[j];
              let catName = this.getcategory(catId);
              if (this.UserdData[i]['categories']){
              this.UserdData[i]['categories'].push({
                'catId':catId,
                'catName':catName
              });
            }else{
              this.UserdData[i]['categories'] = [{}];
              this.spinnerService.hide();
            }
            this.spinnerService.hide();
            }
            this.spinnerService.hide();
          }
          console.log(this.UserdData);
                 
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  
  
  getcategory(id) {
// debugger
    for(let i=0; i<this.categoryData.length; i++){
      if(this.categoryData[i].id === id){
        this.catname = this.categoryData[i].name;
        return this.catname;
        //console.log("catname: " + this.catname[i]);
      }
    }
  }

  categoryData = [];
  getCategoryData() {
    // debugger
    this.spinnerService.show();
    let localUrl = "category/?limit=100000";
    let url = this.server_url + localUrl;

    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  viewreport(id) {
    this._router.navigate(['shopreport/'+id]);
  }
  viewInventory(id) {
    this._router.navigate(['inventory/'+id]);
  }

  onSearchChange(searchValue : string ) {
    this.catName=[];
    // debugger
    this.spinnerService.show();
    let localUrl = "user/?search="+searchValue;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          for(var i = 0;i<this.UserdData.length;i++){
            let obj = this.UserdData[i];
            this.UserdData[i]['categories'] = [];
            for(let j=0; j<obj.category.length; j++ ){
              let catId = obj.category[j];
              let catName = this.getcategory(catId);
              if (this.UserdData[i]['categories']){
              this.UserdData[i]['categories'].push({
                'catId':catId,
                'catName':catName
              });
            }else{
              this.UserdData[i]['categories'] = [{}];
              this.spinnerService.hide();
            }
            this.spinnerService.hide();
            }
            this.spinnerService.hide();
          }          
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    console.log(searchValue);
  
  }
  dropdownSettingsCat = {};

 
  
  filedataimort:any;
  //imageUrl : string ="/importedFiles/targetdataBaseFiles";
  
  
  fileChangeimport(e){
      console.log(e);
  // debugger;
  //let Key = e.target.elements[1].value;
  //let fileList: FileList = e.target.files;
      this.filedataimort = e.target.files[0];
  }
  
  onSubmitfile(e) {
    // let currentUserRoleid = JSON.parse( localStorage.getItem('userRoleid'));
    //   if(currentUserRoleid == "2"){
    //     alert("You Are Not Authenticate to Upload Sales Data")
    //   }else{
    this.spinnerService.show();
    e.preventDefault();
    // debugger;
  
    let formData:FormData = new FormData();
    let file: File = this.filedataimort;
    //let Key = FileList[0];
    //console.log(this.uploadForm);
    formData.append("filename",file);
    // formData.append("fileType",'1');
    // filename: file to be uploaded
    // filetype : 1 or 2 . For vendor upload (1), for product upload (2)

    let httpOptions= {
      headers: new HttpHeaders({ 
        //'content-type':'multipart/form-data',
        'Authorization': 'JWT ' + this.tokens
      })
    };
    let localUrl = "import/vendor/";
    let url = this.server_url+localUrl;
    this.http.post(url,formData,httpOptions)
    .subscribe((res)=>{
      console.log("res"+res);
      if(res) {
        alert("Your "+file.name+"  uploaded successfully..");
        this.spinnerService.hide();
        this.viewUser ='';
        this.getUserData();
         }else {
          alert("File NOT Imported");
          this.spinnerService.hide();
        }
         
    },
    // error => {
    //   if((error['error']['status'] == false) &&  (error['error']['messageCode'] == "QIZ021")) {
    //     alert( "City,Vehicle Versions,Transmission Type and Colour Can Not Be Blank or Wrong"+" "+JSON.stringify(error['error']['data']));this.spinnerService.hide();
    //   } else if((error['error']['status'] == false) && (error['error']['error'])) {
    //     alert( "vin must be unique."+" "+JSON.stringify(error['error']['messageCode']['fields']));this.spinnerService.hide();
    // }
    // else{alert( "Failed to Import.");this.spinnerService.hide(); }
    // }
  );
  
  // }
  }

  allcategoryData = [];
  allcategoryDataDropdown = [];
  allcategoryDataCount;
  getAllCategoryData(userpageNo) {
    //debugger
    this.spinnerService.show();
    let localUrl = "category/?page="+userpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          //debugger
          //  this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.allcategoryDataCount = res['count'];
          var xyz = res;
          this.allcategoryData = this.allcategoryData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if(this.allcategoryData.length < this.allcategoryDataCount) {
            this.getAllCategoryData(userpageNo +1);
          }
          
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          for (let i = 0; i < this.allcategoryData.length; i++) {
            this.allcategoryData[i].id = this.allcategoryData[i].id;
            this.allcategoryData[i].itemName = this.allcategoryData[i].name;
          }

         

         
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  selectedItems = [];
  onItemSelectVersion(item:any){
    console.log(item);
    //console.log(this.selectedItemsStatus);
}
OnItemDeSelectVersion(item:any){
    // console.log(item);
    // console.log(this.selectedItems);
}
onSelectAllVersion(items: any){
    // console.log(items);
}
onDeSelectAllVersion(items: any){
    //console.log(items);
}
StateData;
StateDataCount;
StateDataList;
//  PanelMemberDataCount;
getStateData() {
  debugger
  //this.id = this.route.snapshot.paramMap.get('id');
//  var BATTUTA_KEY = "37cf8f32e2374f064d65ee8c8c9871f0";
  this.spinnerService.show();
  let localUrl = "user/state/";
  let url = this.server_url + localUrl;
  
  this.http.get(url,this.httpOptions)
    .subscribe(
      (res: Response) => {

       // this.StateDataCount = res['count'];
       // var xyz = res;
       // this.StateData = this.StateData.concat(res['results']);
       // if(this.StateData.length < this.StateDataCount) {
       //   this.getStateData(statepageNo +1);
       // }
        this.StateDataList = res;
       // //  this.PanelMemberDataCount = res['count'];
       // for(var s = 0; s<=this.StateData.length;s++ ){
       //   if(this.StateData[s].state != null){
       //     if(this.StateDataList.indexOf(this.StateData[s].state.toLowerCase()) == -1) {
       //       this.StateDataList.push(this.StateData[s].state.toLowerCase());
       //    }
       this.StateDataList.sort((a, b) => {
         if (a < b) return -1;
         else if (a > b) return 1;
         else return 0;
       });

       //   }
       // }
       
        this.spinnerService.hide();
      },
      () => { alert("Error occoured"); this.spinnerService.hide(); }
    );
}

cityData;
   cityDataCount;
   cityDataList=[];
  //  PanelMemberDataCount;
   getCityData(state) {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
//  var BATTUTA_KEY = "37cf8f32e2374f064d65ee8c8c9871f0";
     this.spinnerService.show();
    //  let localUrl = "user/?limit=10000000";
    //  let url = this.server_url + localUrl;
     
     this.http.get("assets/cities.json",this.httpOptions)
       .subscribe(
         (res: Response) => {

          // this.StateDataCount = res['count'];
          // var xyz = res;
          // this.StateData = this.StateData.concat(res['results']);
          // if(this.StateData.length < this.StateDataCount) {
          //   this.getStateData(statepageNo +1);
          // }
           this.cityData = res;
           this.cityDataList = this.cityData.filter(
            city => city.state == state[0].toUpperCase() + state.slice(1));
            this.cityDataList.sort((a, b) => {
              if (a.name < b.name) return -1;
              else if (a.name > b.name) return 1;
              else return 0;
            });
          // //  this.PanelMemberDataCount = res['count'];
          // for(var s = 0; s<=this.cityData.length;s++ ){
          //   if(this.cityData[s].city != null){
          //     if(this.cityDataList.indexOf(this.cityData[s].city.toLowerCase()) == -1) {
          //       this.cityDataList.push(this.cityData[s].city.toLowerCase());
          //    }
              

          //   }
          // }
          
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   filterDataCount(form: any) {
  debugger;
// if(form.value.mydaterangecount != null){


  this.spinnerService.show();
 console.log('you submitted value:', form.value);

 let filterBody:any = {};

 if(form.value.Statecount != null && form.value.Statecount != "null"){
  filterBody.state = form.value.Statecount.toLowerCase();;
}
// else{filterBody.state = null}

if(form.value.SelectedCityCount != null && form.value.SelectedCityCount != "null"){
filterBody.city = form.value.SelectedCityCount.toLowerCase();;
}
// else{filterBody.city = null}
 

if(form.value.mydaterangecount != null){
var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
// this.filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
// this.filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
filterBody.fromDate = Math.round((new Date(form.value.mydaterangecount.beginJsDate.setHours(0,0,0,0) - tzoffset)).getTime()/1000);
filterBody.toDate = Math.round((new Date(form.value.mydaterangecount.endJsDate.setHours(23,59,59,999) - tzoffset)).getTime()/1000);


}

if(form.value.checkValueRegistered == true){
  filterBody.checkValueRegistered = form.value.checkValueRegistered;
  }


this.filterbodyPagination = filterBody;

// let localUrl = "stat/brand/?city="+filterBody.city+"&state="+filterBody.state+"&fromDate="+filterBody.fromDate+"&toDate="+filterBody.toDate+"&user_code="+filterBody.searchShopcode;
let localUrl = "user/?";

// // let localUrl = "stat/ticket/?status=3&"
if (filterBody.state) {
localUrl = localUrl + "state=" +filterBody.state + "&"
}
if (filterBody.city) {
localUrl = localUrl + "city=" +filterBody.city + "&"
}
if (filterBody.fromDate && filterBody.toDate) {
localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
}
if(filterBody.checkValueRegistered == true){
  localUrl = localUrl +"is_logged_in=1";
  }

// // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
let url = this.server_url+localUrl;


this.http.get(url, this.httpOptions)
.subscribe(
  (res: Response) => {
    this.UserdData = res['results'];
    
    this.UserDataCount = res['count'];
    for(var i = 0;i<this.UserdData.length;i++){
      let obj = this.UserdData[i];
      this.UserdData[i]['categories'] = [];
      for(let j=0; j<obj.category.length; j++ ){
        let catId = obj.category[j];
        let catName = this.getcategory(catId);
        if (this.UserdData[i]['categories']){
        this.UserdData[i]['categories'].push({
          'catId':catId,
          'catName':catName
        });
      }else{
        this.UserdData[i]['categories'] = [{}];
        this.spinnerService.hide();
      }
      this.spinnerService.hide();
      }
      this.spinnerService.hide();
    }
    console.log(this.UserdData);
           
    this.spinnerService.hide();

  },
() => {this.spinnerService.hide(); alert( "Error occoured");}
);

// } else{
//   this.spinnerService.hide();
//   alert("Please select One");

// }// if else form.value close
this.spinnerService.hide();

// }else{
//   alert("Please select date.")
// }
}
onResetFiltercount(){
  this.getUserData();
  this.filterbodyPagination=[];
}

  ngOnInit() {
    this.getCategoryData();
    this.getAllCategoryData(this.userpageNo);
     this.getUserData();
     this.getStateData();
  
   

    this.dropdownSettingsCat = {
      singleSelection: false, 
      text:"Select",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: false,
      classes:"myclass custom-class"
    };
  }

}
