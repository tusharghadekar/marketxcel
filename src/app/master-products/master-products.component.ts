import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {ModalModule} from "ngx-modal";
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-master-products',
  templateUrl: './master-products.component.html',
  styleUrls: ['./master-products.component.css']
})
export class MasterProductsComponent implements OnInit {

  public viewProduct:string;

  server_url : string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };

 constructor(private _router: Router,private modal: ModalModule,private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService,private location: Location, private user: UserService) { }

  ShowProductTable(){
    this.viewProduct = 'addProduct';
  }
  Showstore() {
    //this.viewShops = 'addShops';
    this.location.back();
  }

  public addProduct(e): void{
    e.preventDefault();
    debugger;
    
    let name =  e.target.elements[0].value;
    //let code =  e.target.elements[1].value;
    let brand = e.target.elements[2].value;
    let category = e.target.elements[1].value;
    //let price = e.target.elements[4].value;
    //let size = e.target.elements[5].value;
    //let type = e.target.elements[6].value;
    let description = e.target.elements[3].value;

 const addProductObject = {
  name:name,
  description:description,
  brand:brand,
  category : category,
  specification: ""
 }
 debugger;
 let localUrl = "product/";
 let url = this.server_url+localUrl;
 this.http.post(url, JSON.stringify(addProductObject), this.httpOptions)
 .subscribe(
    (res) => {
      if(res) {
       alert("You added new Product successfully..");
       this.viewProduct ='';
      // $("#myModal").modal("hide");
       this.getProductData();
          }else {
            alert("Product NOT Added");
          }
    },
    err => {alert( "Error occoured");
   // this.viewAuditor ='';
  }
    );
       
}

// open() {
//   this.modal = this.modal.open();

//   this.modal.afterClosed().subscribe(result => {
//       this.dialogRef = null;
//   });
// }

productpageNo: number = 1;
  pageChange(pageNo: any) {
    debugger
    this.productpageNo = pageNo;
    console.log("pageNo: " + this.productpageNo);

    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
    let id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "product/?brand="+id+"&page="+this.productpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ProductData = res['results'];
          this.ProductDataCount = res['count'];

          this.ProductData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });

        this.ProductData.forEach(obj => {

          let localUrl = "brand/"+obj.brand;
          //let localUrl = "brand/";
          let url = this.server_url + localUrl;
          
          this.http.get(url,this.httpOptions)
            .subscribe(
              (res: Response) => {
                var xyz=res;
                obj.brandname=xyz["name"];
                this.spinnerService.hide();
              },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
          
      });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

onIsActiveChange(id:number, isActive:string){
  //
  if(confirm("Are you sure ?")){
    if(isActive == 'true')
    {
      isActive = 'false';
    }else if(isActive == 'false'){
      isActive = 'true';
    }
    let localUrl = "product/"+id;
    let url = this.server_url+localUrl;

    
    const deactiveProductObject = { is_active : isActive }
    let jsonProductBody = JSON.stringify(deactiveProductObject);

this.http.patch(url, jsonProductBody, this.httpOptions).subscribe(
(res) => {
  if(res)
//this.categoryData = res['data'];
alert("You have changed staus  successfully..");
//this.viewopsVehicleSalesData = '';
this.getProductData();
});
}else{alert("You made no change"); 
// this.getopsVehicleAuditsMasterData(this.VehicleAuditspageNo); this.viewopsVehicleAudits = '';
}
}

updateProduct(beforeUpdateProductObj) {
  debugger
    this.beforeUpdateProductObj = { 
      "name": beforeUpdateProductObj.name,
      "description": beforeUpdateProductObj.description,
      "category": beforeUpdateProductObj.category,
      "brand" : beforeUpdateProductObj.brand,
      "is_active":beforeUpdateProductObj.is_active
   };
   let localUrl = "product/"+beforeUpdateProductObj.id+"/";
   let url = this.server_url+localUrl;
  
   this.http.patch(url, JSON.stringify(this.beforeUpdateProductObj), this.httpOptions).subscribe(
     (res) => {
       if(res){
       this.beforeUpdateProductObj = {
        "id": res['id'],
        "name": res['name'],
        "description": res['description'],
        "brand" : res['brand'],
        "category": res['category'],
        "is_active": res['is_active']
       };
       alert("Successfully Updated");
      this.viewProduct =''; 
      this.getProductData();      
     }else{alert("error Occured")}},
         );
      
       
  } // Update Auditor functionality close
  beforeUpdateProductObj;
  // Editing Auditor function 
  editProduct(ProductData){
    
           if(confirm("You want to edit Product ?")){
          debugger;
           this.viewProduct = 'editProduct';
           let localUrl = "product/"+ProductData.id+"/";
           let url = this.server_url+localUrl;
           const checkproductObject = { id : ProductData.id }
  
   this.http.patch(url,JSON.stringify(checkproductObject), this.httpOptions).subscribe(
   (res) => {
     if(res)
     this.beforeUpdateProductObj = {
       "id": res['id'], 
      "name": res['name'],
      "description": res['description'],
      "category" : res['category'],
      "brand" : res['brand'],
      "is_active": res['is_active']
      
     };
  
         }, err =>{alert("You made no change");}
       );
     }else{}
    
   }

   deleteProduct(ProductData){
    if(confirm("You want to Delete Product?")){
   debugger;
    let localUrl = "product/"+ProductData.id+"/";
    let url = this.server_url+localUrl;
  
  this.http.delete(url,this.httpOptions).subscribe(
  (res) => {
  alert("You Deleted Product");
  this.viewProduct="";
  this.getProductData();
  }, err =>{alert("You made no change");}
  );
  }else{alert("error occured");}
  
  }

   catname;
  getcategoryName(id) {
    //debugger
        for(let i=0; i<this.categoryData.length; i++){
          if(this.categoryData[i].id === id){
            this.catname = this.categoryData[i].name;
            return this.catname;
            //console.log("catname: " + this.catname[i]);
          }
        }
      }
      brandname;
      getbrandName(id) {
        //debugger
            for(let i=0; i<this.BrandData.length; i++){
              if(this.BrandData[i].id === id){
                this.brandname = this.BrandData[i].name;
                return this.brandname;
                //console.log("catname: " + this.catname[i]);
              }
            }
          }
    id;  
   ProductData;
   ProductDataCount;
   getProductData() {
     debugger
     this.id = this.route.snapshot.paramMap.get('id');
     this.spinnerService.show();
     let localUrl = "product?brand="+this.id;
     //let localUrl = "brand/";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.ProductData = res['results'];
           this.ProductDataCount = res['count'];

           this.ProductData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });

        this.ProductData.forEach(obj => {

          let localUrl = "brand/"+obj.brand;
          //let localUrl = "brand/";
          let url = this.server_url + localUrl;
          
          this.http.get(url,this.httpOptions)
            .subscribe(
              (res: Response) => {
                var xyz=res;
                obj.brandname=xyz["name"];
                this.spinnerService.hide();
              },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
          
      });
           this.spinnerService.hide();
         },
         err => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

  BrandData;
  brandDataCount;
  getBrandData() {
    debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  categoryData = [];
catDataCount;
  getCategoryData() {
    debugger
    this.spinnerService.show();
    let localUrl = "category/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  allcategoryData = [];
  allcategoryDataCount;

  getAllCategoryData(productpageNo) {
    //debugger
    this.spinnerService.show();
    let localUrl = "category/?page="+productpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          //debugger
          //  this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.allcategoryDataCount = res['count'];
          var xyz = res;
          this.allcategoryData = this.allcategoryData.concat(res['results']);
          if(this.allcategoryData.length < this.allcategoryDataCount) {
            this.getAllCategoryData(productpageNo +1);
          }
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }


  allbrandData = [];
  allbrandDataCount;

  getAllbrandData(productpageNo) {
    //debugger
    this.spinnerService.show();
    let localUrl = "brand/?page="+productpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          //debugger
          //  this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.allbrandDataCount = res['count'];
          var xyz = res;
          this.allbrandData = this.allbrandData.concat(res['results']);
          if(this.allbrandData.length < this.allbrandDataCount) {
            this.getAllbrandData(productpageNo +1);
          }
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  // filteredbrandData=[];
  // filteredbrandDataCount
  // getfilteredbrandData(id,productpageNo) {
  //   debugger
  //   this.spinnerService.show();
  //   let localUrl = "brand/?category="+id+"&page="+this.productpageNo;
  //   let url = this.server_url + localUrl;
    
  //   this.http.get(url,this.httpOptions)
  //     .subscribe(
  //       (res: Response) => {
  //         debugger
  //         //  this.categoryData.push(res['results'])
  //         // this.categoryData = this.categoryData.concat(res['results']);
  //         this.allbrandDataCount = res['count'];
  //         var xyz = res;
  //         this.allbrandData = this.allbrandData.concat(res['results']);
  //         if(this.allbrandData.length < this.allbrandDataCount) {
  //           this.getAllbrandData(productpageNo +1);
  //         }
  //         this.spinnerService.hide();
  //       },
  //       err => { alert("Error occoured"); this.spinnerService.hide(); }
  //     );
  // }

  // filteredBrandObj;

  // onSelectCatNameChange(value: any){
  //   debugger;
  // //this.filteredBrandObj.splice(0,this.filteredBrandObj.length);
  // for(let i=0; i<this.allbrandData.length ;i++){
  //   if(this.allbrandData[i].category == value){
  //     this.filteredBrandObj.push(this.allbrandData[i])
  //   }
  // }
  
  // }

  uploadForm = new FormGroup ({
    //file1: new FormControl()
  });
  filedata:any;
  //imageUrl : string ="/importedFiles/targetdataBaseFiles";
  
  
  fileChange(e){
      console.log(e);
  debugger;
  //let Key = e.target.elements[1].value;
  //let fileList: FileList = e.target.files;
      this.filedata = e.target.files[0];
  }
  
  onSubmit(e) {
    // let currentUserRoleid = JSON.parse( localStorage.getItem('userRoleid'));
    //   if(currentUserRoleid == "2"){
    //     alert("You Are Not Authenticate to Upload Sales Data")
    //   }else{
    this.spinnerService.show();
    e.preventDefault();
    debugger;
  
    let formData:FormData = new FormData();
    let file: File = this.filedata;
    //let Key = FileList[0];
    //console.log(this.uploadForm);
    formData.append("filename",file);
    formData.append("fileType",'2');
    // filename: file to be uploaded
    // filetype : 1 or 2 . For vendor upload (1), for product upload (2)

    let httpOptions= {
      headers: new HttpHeaders({ 
        //'content-type':'multipart/form-data',
        'Authorization': 'JWT '+ this.tokens
      })
    };
    let localUrl = "import/product/";
    let url = this.server_url+localUrl;
    this.http.post(url,formData,httpOptions)
    .subscribe((res)=>{
      console.log("res"+res);
      if(res) {
        alert("Your "+file.name+"  uploaded successfully..");
        this.spinnerService.hide();
        this.viewProduct ='';
        this.getProductData();
         }else {
          alert("File NOT Imported");
          this.spinnerService.hide();
        }
         
    },
    // error => {
    //   if((error['error']['status'] == false) &&  (error['error']['messageCode'] == "QIZ021")) {
    //     alert( "City,Vehicle Versions,Transmission Type and Colour Can Not Be Blank or Wrong"+" "+JSON.stringify(error['error']['data']));this.spinnerService.hide();
    //   } else if((error['error']['status'] == false) && (error['error']['error'])) {
    //     alert( "vin must be unique."+" "+JSON.stringify(error['error']['messageCode']['fields']));this.spinnerService.hide();
    // }
    // else{alert( "Failed to Import.");this.spinnerService.hide(); }
    // }
  );
  
  // }
  }

  onSearchChange(searchValue : string ) {
    debugger
    this.spinnerService.show();

    let id = this.route.snapshot.paramMap.get('id');

    let localUrl = "product?brand="+id+"&name="+searchValue;
     //let localUrl = "brand/";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.ProductData = res['results'];
           this.ProductDataCount = res['count'];

           this.ProductData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });

        this.ProductData.forEach(obj => {

          let localUrl = "brand/"+obj.brand;
          //let localUrl = "brand/";
          let url = this.server_url + localUrl;
          
          this.http.get(url,this.httpOptions)
            .subscribe(
              (res: Response) => {
                var xyz=res;
                obj.brandname=xyz["name"];
                this.spinnerService.hide();
              },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
          
      });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    console.log(searchValue);
  
  }

  ngOnInit() {
    this.getBrandData();
    this.getCategoryData();
    this.getProductData();
    this.getAllCategoryData(this.productpageNo);
    this.getAllbrandData(this.productpageNo);
  }

}
