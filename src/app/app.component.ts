import { Component, OnInit, isDevMode } from '@angular/core';
import { environment } from '../environments/environment';
import {Router} from '@angular/router';


declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Market Excel';
  public url: string = "";

  constructor(private router:Router) { }

  ngOnInit() {
    //this.router.navigate(['/login-form'])
    this.url = this.router.url;
    if (isDevMode()) {
     // console.log('Your in Development!');
    } else {
     // console.log('Your in  Production!');
    }
  }

}
