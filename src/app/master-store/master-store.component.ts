import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-master-store',
  templateUrl: './master-store.component.html',
  styleUrls: ['./master-store.component.css']
})
export class MasterStoreComponent implements OnInit {

  public viewStore:string;

  server_url : string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };

  constructor(private _router: Router,private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService,private location: Location, private user: UserService) { }

  ShowStoreTable(){
    this.viewStore = 'addStore';
  }
  redirect(id) {
    this._router.navigate(['products/'+id]);
  }
  ShowCat() {
    //this.viewShops = 'addShops';
    this.location.back();
  }

  public addBrand(e): void{
    e.preventDefault();
    debugger;
    
    let name =  e.target.elements[0].value;
    let description =  e.target.elements[1].value;
    let category = e.target.elements[2].value;

 const addBrandObject ={
  name:name,
  description:description,
   category : category,
 }
 debugger;
 let localUrl = "brand/";
 let url = this.server_url+localUrl;
 this.http.post(url, JSON.stringify(addBrandObject), this.httpOptions)
 .subscribe(
    (res) => {
      if(res) {
       alert("You added new Brand successfully..");
       this.viewStore ='';
       this.getBrandData();
          }else {
            alert("Brand NOT Added");
          }
    },
    err => {alert( "Error occoured");
   // this.viewAuditor ='';
  }
    );
       
}

onIsActiveChange(id:number, isActive:string){
  //
  if(confirm("Are you sure ?")){
    if(isActive == 'true')
    {
      isActive = 'false';
    }else if(isActive == 'false'){
      isActive = 'true';
    }
    let localUrl = "brand/"+id;
    let url = this.server_url+localUrl;

    
    const deactiveBrandObject = { is_active : isActive }
    let jsonBrandBody = JSON.stringify(deactiveBrandObject);

this.http.patch(url, jsonBrandBody, this.httpOptions).subscribe(
(res) => {
  if(res)
//this.categoryData = res['data'];
alert("You have changed staus  successfully..");
//this.viewopsVehicleSalesData = '';
this.getBrandData();
});
}else{alert("You made no change"); 
// this.getopsVehicleAuditsMasterData(this.VehicleAuditspageNo); this.viewopsVehicleAudits = '';
}
}

updateBrand(beforeUpdateBrandObj) {
  debugger
    this.beforeUpdateBrandObj = { 
      "name": beforeUpdateBrandObj.name,
      "description": beforeUpdateBrandObj.description,
      "category": beforeUpdateBrandObj.category,
      "is_active":beforeUpdateBrandObj.is_active
   };
   let localUrl = "brand/"+beforeUpdateBrandObj.id+"/";
   let url = this.server_url+localUrl;
  
   this.http.put(url, JSON.stringify(this.beforeUpdateBrandObj), this.httpOptions).subscribe(
     (res) => {
       if(res){
       this.beforeUpdateBrandObj = {
        "id": res['id'],
        "name": res['name'],
        "description": res['description'],
        "category": res['category'],
        "is_active": res['is_active']
       };
  alert("Successfully Updated");
      this.viewStore =''; 
      this.getBrandData();      
     }else{alert("error Occured")}},
         );
      
       
  } // Update Auditor functionality close
  beforeUpdateBrandObj;
  // Editing Auditor function 
  editBrand(BrandData){
    
           if(confirm("You want to edit Brand ?")){
          debugger;
           this.viewStore = 'editStore';
           let localUrl = "brand/"+BrandData.id+"/";
           let url = this.server_url+localUrl;
           const checkcatObject = { id : BrandData.id }
  
   this.http.patch(url,JSON.stringify(checkcatObject), this.httpOptions).subscribe(
   (res) => {
     if(res)
     this.beforeUpdateBrandObj = {
       "id": res['id'], 
      "name": res['name'],
      "description": res['description'],
      "category" : res['category'],
      "is_active": res['is_active']
      
     };
  
         }, err =>{alert("You made no change");}
       );
     }else{}
    
   }

deleteBrand(BrandData){
  if(confirm("You want to Delete Brand?")){
 debugger;
  let localUrl = "brand/"+BrandData.id+"/";
  let url = this.server_url+localUrl;

this.http.delete(url,this.httpOptions).subscribe(
(res) => {
alert("You Deleted Brand");
this.viewStore="";
this.getBrandData();
}, err =>{alert("You made no change");}
);
}else{alert("error occured");}

}

brandpageNo: number = 1;
  pageChange(pageNo: any) {
    debugger
    this.brandpageNo = pageNo;
    console.log("pageNo: " + this.brandpageNo);
    
    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
    let id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand/?category="+id+"&page="+this.brandpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];

          this.BrandData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  onSearchChange(searchValue : string ) {
    debugger
    this.spinnerService.show();
    let localUrl = "brand/?name="+searchValue;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];

          this.BrandData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    console.log(searchValue);
  
  }


id:any;
  BrandData;
  brandDataCount;
  getBrandData() {
    debugger
    this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand?category="+this.id;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];

          this.BrandData.forEach(obj => {

            let localUrl = "category/"+obj.category;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.catname=xyz["name"];
                  this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
        });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  categoryData;
  categoryDataCount;
  getCategoryData(brandpageNo) {
    debugger
    this.spinnerService.show();
    let localUrl = "category/?page="+brandpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {

           this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.categoryDataCount = res['count'];
          var xyz = res;
          if(this.categoryData.length < this.categoryDataCount) {
            this.getCategoryData(brandpageNo +1);
          }
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
catname;
  getcategory(id) {
    debugger
        for(let i=0; i<this.categoryData.length; i++){
          if(this.categoryData[i].id === id){
            this.catname = this.categoryData[i].name;
            return this.catname;
            //console.log("catname: " + this.catname[i]);
          }
        }
      }
  ngOnInit() {
    this.getBrandData();
    this.getCategoryData(this.brandpageNo);
  }

}
