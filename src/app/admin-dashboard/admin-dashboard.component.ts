import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { Router } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { EChartOption } from 'echarts';
declare var $: any;
import 'rxjs/Rx';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';



@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})

export class AdminDashboardComponent implements OnInit {
  Math: any;
  public userName: string;
  public isLogin: string;
  
  public DashboardDataa={};
  server_url: string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };

  constructor(private _router: Router, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient) {
    this.Math = Math;
   
  }
  public chartOptionbar = {};
  public chartOptions:any = {};
  public chartOptionslocation:any = {};
  // mydaterange = new Date().getDate()+"."+new Date().getMonth()+1 +"."+new Date().getFullYear();
  mydaterange = new Date();


  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd.mm.yyyy',
    sunHighlight: true,
    height: '40px',
    width: '100%',
    inline: false,
    alignSelectorRight: false,
    indicateInvalidDateRange: true,
    
  };


  

  
  barChartData=[];
  DataobjectTopbrands;
  barChartLabels=[];
  public barChartLabelss;
  public barChartDataa: ChartDataSets[];

  filterData(form: any) {
    debugger
    this.spinnerService.show();
   console.log('you submitted value:', form.value);

   let filterBody:any = {};

   if(form.value.State != null){
    filterBody.state = form.value.State.toLowerCase();
 }else{filterBody.state = null}

 if(form.value.SelectedCity != null){
  filterBody.city = form.value.SelectedCity.toLowerCase();
}else{filterBody.city = null}

if(form.value.Selectedcategory != null){
  filterBody.category = form.value.Selectedcategory;
}else{filterBody.category = null}

if(form.value.brand != null){
  filterBody.brand = form.value.brand;
}else{filterBody.brand = null}

if(form.value.searchShopcode != null){
  filterBody.searchShopcode = form.value.searchShopcode;
}else{filterBody.searchShopcode = null}
   

if(form.value.mydaterange != null){
  var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
  // this.filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
  // this.filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
 filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
 filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
}else{filterBody.fromDate = null;filterBody.toDate = null;}
debugger
// let localUrl = "stat/brand/?city="+filterBody.city+"&state="+filterBody.state+"&fromDate="+filterBody.fromDate+"&toDate="+filterBody.toDate+"&user_code="+filterBody.searchShopcode;
let localUrl = "stat/brand/?";

// let localUrl = "stat/ticket/?status=3&"
if (filterBody.state) {
  localUrl = localUrl + "state=" +filterBody.state + "&"
}
if (filterBody.city) {
  localUrl = localUrl + "city=" +filterBody.city + "&"
}
if (filterBody.category) {
  localUrl = localUrl + "category=" +filterBody.category + "&"
}
if (filterBody.brand) {
  localUrl = localUrl + "brand=" +filterBody.brand + "&"
}
if (filterBody.fromDate && filterBody.toDate) {
  localUrl = localUrl +"fromDate="+filterBody.fromDate+"&"+"toDate="+filterBody.toDate;
}

// let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
let url = this.server_url+localUrl;


this.http.get(url, this.httpOptions)
  .subscribe(
    (res:Response) => {
    debugger
    this.DataobjectTopbrands = res;

    this.barChartLabels = Object.keys(this.DataobjectTopbrands)
  .sort((a, b) => this.DataobjectTopbrands[b] - this.DataobjectTopbrands[a])
  .slice(0, 10);

this.barChartData = this.barChartLabels.map(key => this.DataobjectTopbrands[key]);
    //return this.DataobjectTopbrands;
    //this.totalcustomer = res['data']['surveysCount'][0]['totalCustomer'];
    //this.totalissues = this.DataobjectTopissues.graphData.length;
    
    // this.barChartLabelsDelivery=[];
    // this.dataArray=[];
    // this.pphDelivery=[];

    // for(let i=0; i< this.DataobjectTopissues.graphData.length; i++){
    //  this.barChartLabelsDelivery.push(this.DataobjectTopissues.graphData[i]['master_problem_sub_categories_name']);

     
     
    //  for(let j=0; j< this.DataobjectTopissues.surveysCount.length; j++){
    //      this.pphDelivery = (this.DataobjectTopissues.graphData[i]['customerReportedCnt']/this.DataobjectTopissues.surveysCount[j]['totalCustomer'])*100;
    //      this.pphDelivery = this.pphDelivery.toFixed(1);
         
    //      this.dataArray.push(this.pphDelivery);
    //     }
       
    //   }
      // for(let k =0; k<this.DataobjectTopissues.graphData.length; k++){
      //   this.totalissues += this.DataobjectTopissues.graphData[k].customerReportedCnt;
     
      //  }
      //  this.totalpph = ((this.totalissues/this.totalcustomer)*100).toFixed(1);
   

 
 this.getbargraph();
   
    
     this.spinnerService.hide(); 
  },
  () => {this.spinnerService.hide(); alert( "Error occoured");}
  );

 // } else{
 //   this.spinnerService.hide();
 //   alert("Please select One");

 // }// if else form.value close
 this.spinnerService.hide();

}
public EChartOption: any = {};

public barChartOptions: ChartOptions;
public barChartType: ChartType;
public barChartLegend;
  getbargraph(){
    debugger

    this.barChartOptions = {
      responsive: true,
      // We use these empty structures as placeholders for dynamic theming.
      scales: { xAxes: [{}], yAxes: [{}] },
      // plugins: {
      //   datalabels: {
      //     anchor: 'end',
      //     align: 'end',
      //   }
      // }
    };
    this.barChartLabelss = this.barChartLabels;
    this.barChartType = 'bar';
    this.barChartLegend = true;
  
    this.barChartDataa = [
      { data: this.barChartData, label: 'Brand' }
    ];
  //  this.EChartOption  = {
  //     xAxis: {
  //       type: 'category',
  //       data: this.barChartLabels
  //     },
  //     yAxis: {
  //       type: 'value'
  //     },
  //     legend: {
  //       data:['Brand']
  //   },
  //     series: [{
  //       name:'Brand',
  //       data: this.barChartData,
  //       type: 'bar',
      
  //     itemStyle: {
  //       normal: {
  //           // color: function(params) {
  //           //     // build a color map as your need.
  //           //     var colorList = [
  //           //       '#C1232B','#B5C334','#FCCE10','#E87C25','#27727B',
  //           //        '#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD',
  //           //        '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
  //           //     ];
  //           //     return colorList[params.dataIndex]
  //           // },
  //           label: {
  //               show: true,
  //               position: 'top',
  //               formatter: '{c}'
  //           }
  //       }
  //   }
  // },
  //     // {
  //     //   data: [47, 9, 28, 54, 77],
  //     //   type: 'bar'
  //     // }
  //   ]
  //   }

  }
 
//   getShopPie(){
//   this.chartOptions = {
//     title : {
//       text: 'Store Wise',
//   },
//   tooltip : {
//     trigger: 'item',
//     formatter: "{b} : {c} ({d}%)"
// },
//     // xAxis: {
//     //   type: 'category',
//     //   data: ['Store1', 'Store2', 'Store3', 'Store4','Store5']
//     // },
//     // yAxis: {
//     //   type: 'value'
//     // },
//     series: [{
//       data:[
//         {value:335, name:'Store1'},
//         {value:310, name:'Store2'},
//         {value:234, name:'Store3'},
//         {value:135, name:'Store4'},
//         {value:1548, name:'Store5'}
//     ],
//     name:'Store Wise',
//       type: 'pie'
//     },
//     // {
//     //   data: [47, 9, 28, 54, 77],
//     //   type: 'pie'
//     // }
//   ]
//   }
// }
//   LocationPie(){
//   this.chartOptionslocation = {
//     title : {
//       text: 'Location Wise',
//   },
//   tooltip : {
//     trigger: 'item',
//     formatter: "{b} : {c} ({d}%)"
// },
    
//     series: [{
//       data:[
//         {value:335, name:'Store1'},
//         {value:310, name:'Store2'},
//         {value:234, name:'Store3'},
//         {value:135, name:'Store4'},
//         {value:1548, name:'Store5'}
//     ],
//     name:'Location Wise',
//       type: 'pie'
//     },
//     // {
//     //   data: [47, 9, 28, 54, 77],
//     //   type: 'pie'
//     // }
//   ]
//   }
// }
  // chartOptionss = {
  //   scaleShowVerticalLines: true,
  //   responsive: true,
  //   layout: {
  //     padding: {
  //       left: 0,
  //       right: 0,
  //       top: 30,
  //       bottom: 0
  //     },
  //   },
    
 
  // }
  // chartOptionsss = {
  //   scaleShowVerticalLines: true,
  //   responsive: true,
  //   layout: {
  //     padding: {
  //       left: 0,
  //       right: 0,
  //       top: 30,
  //       bottom: 0
  //     },
  //   },
    
 
  // }

  // labels =  ['Store1', 'Store2', 'Store3', 'Store4','Store5'];

  // // STATIC DATA FOR THE CHART IN JSON FORMAT.
  // chartData = [
  //   {
  //     label: '2017',
  //     data: [21, 56, 4, 31, 45] 
  //   },
  //   { 
  //     label: '2018',
  //     data: [47, 9, 28, 54, 77]
  //   }
    
  // ];

  // CHART COLOR.
  // colors = [
  //   { // 1st Year.
  //     backgroundColor: 'rgba(77,83,96,0.2)'
  //   },
  //   { // 2nd Year.
  //     backgroundColor: 'rgba(30, 169, 224, 0.8)'
  //   }
  // ]
  
  // CHART CLICK EVENT.
  onChartClick(event) {
    console.log(event);
  }

  

 
  
onResetFilter(){
  //this.DashboardDataa = [];
 
}

viewreport(id) {
  this._router.navigate(['shopreport/'+id]);
}
onResetFiltercount(){
  //this.DashboardDataa = [];
  this.getUserData();
    // this.getProductData();
    this.getPanelMemberData();
    this.getUserDataTodays();
    this.getUserDataTodaysAppUsed();
    this.getRegististeredUserData();
    this.getNonPanelMemberData();

    this.downloadxcelFromDate=[];
    this.downloadxcelToDate=[];
    this.filterBodyForDownloadCode=[];
 
}

onRefresh(){
  //this.DashboardDataa = [];
  this.ngOnInit();
 
}
downloadxcelFromDate;
downloadxcelToDate;

filterBodyForDownloadCode;

filterDataCount(form: any) {
  debugger;
// if(form.value.mydaterangecount != null){


  this.spinnerService.show();
 console.log('you submitted value:', form.value);

 let filterBody:any = {};

 if(form.value.Statecount != null && form.value.Statecount != "null"){

  filterBody.state = encodeURIComponent(form.value.Statecount.toLowerCase());
  
}
// else{filterBody.state = null}

if(form.value.SelectedCityCount != null && form.value.SelectedCityCount != "null"){
filterBody.city = encodeURIComponent(form.value.SelectedCityCount.toLowerCase());
}
// else{filterBody.city = null}
 

if(form.value.mydaterangecount != null){
var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
// this.filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
// this.filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
filterBody.fromDate = Math.round((new Date(form.value.mydaterangecount.beginJsDate.setHours(0,0,0,0) - tzoffset)).getTime()/1000);
filterBody.toDate = Math.round((new Date(form.value.mydaterangecount.endJsDate.setHours(23,59,59,999) - tzoffset)).getTime()/1000);

this.downloadxcelFromDate =filterBody.fromDate;
this.downloadxcelToDate = filterBody.toDate;

}

this.filterBodyForDownloadCode = filterBody;

// else{filterBody.fromDate = null;filterBody.toDate = null;}
debugger

// this.getUserDataCountfilter(filterBody);
this.getPanelMemberDatafilter(filterBody);
// this.getUserDataTodaysAppUsedcounter(filterBody);
// this.getUserDataTodayscounter(filterBody);
this.getRegististeredUserDataFilter(filterBody);
this.getNonPanelMemberDatafilter(filterBody);
// this.downloadExcel(filterBody);
// let localUrl = "stat/brand/?city="+filterBody.city+"&state="+filterBody.state+"&fromDate="+filterBody.fromDate+"&toDate="+filterBody.toDate+"&user_code="+filterBody.searchShopcode;
// let localUrl = "user/?";

// // let localUrl = "stat/ticket/?status=3&"
// if (filterBody.state) {
// localUrl = localUrl + "state=" +filterBody.state + "&"
// }
// if (filterBody.city) {
// localUrl = localUrl + "city=" +filterBody.city + "&"
// }
// if (filterBody.fromDate && filterBody.toDate) {
// localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
// }

// // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
// let url = this.server_url+localUrl;


// this.http.get(url, this.httpOptions)
// .subscribe(
//   (res:Response) => {
//   debugger
//   this.DataobjectTopbrands = res;

//   this.barChartLabels = Object.keys(this.DataobjectTopbrands)
// .sort((a, b) => this.DataobjectTopbrands[b] - this.DataobjectTopbrands[a])
// .slice(0, 10);

// this.barChartData = this.barChartLabels.map(key => this.DataobjectTopbrands[key]);
//   //return this.DataobjectTopbrands;
//   //this.totalcustomer = res['data']['surveysCount'][0]['totalCustomer'];
//   //this.totalissues = this.DataobjectTopissues.graphData.length;
  
//   // this.barChartLabelsDelivery=[];
//   // this.dataArray=[];
//   // this.pphDelivery=[];

//   // for(let i=0; i< this.DataobjectTopissues.graphData.length; i++){
//   //  this.barChartLabelsDelivery.push(this.DataobjectTopissues.graphData[i]['master_problem_sub_categories_name']);

   
   
//   //  for(let j=0; j< this.DataobjectTopissues.surveysCount.length; j++){
//   //      this.pphDelivery = (this.DataobjectTopissues.graphData[i]['customerReportedCnt']/this.DataobjectTopissues.surveysCount[j]['totalCustomer'])*100;
//   //      this.pphDelivery = this.pphDelivery.toFixed(1);
       
//   //      this.dataArray.push(this.pphDelivery);
//   //     }
     
//   //   }
//     // for(let k =0; k<this.DataobjectTopissues.graphData.length; k++){
//     //   this.totalissues += this.DataobjectTopissues.graphData[k].customerReportedCnt;
   
//     //  }
//     //  this.totalpph = ((this.totalissues/this.totalcustomer)*100).toFixed(1);
 


// // this.getbargraph();
 
  
//    this.spinnerService.hide(); 
// },
// () => {this.spinnerService.hide(); alert( "Error occoured");}
// );

// } else{
//   this.spinnerService.hide();
//   alert("Please select One");

// }// if else form.value close
this.spinnerService.hide();

// }else{
//   alert("Please select date.")
// }
}


    
  //    cancel() {
  //     // Simply navigate back to reminders view
  //     this._router.navigate(['./dashboard']);   
  //  }

  getBrandStatData() {
    // debugger
    this.spinnerService.show();
    let localUrl = "stat/brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DataobjectTopbrands = res['results'];
          //this.catDataCount = this.categoryData.length;
          // this.getbargraph();
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  
  categoryData = [];
  catDataCount;
    getCategoryData() {
      // debugger
      this.spinnerService.show();
      let localUrl = "category/?limit=100000";
      let url = this.server_url + localUrl;
      
      this.http.get(url,this.httpOptions)
        .subscribe(
          (res: Response) => {
            this.categoryData = res['results'];
            this.catDataCount = this.categoryData.length;
            this.spinnerService.hide();
          },
          () => { alert("Error occoured"); this.spinnerService.hide(); }
        );
    }

    BrandData;
  brandDataCount;
  getBrandData(catId) {
    // debugger
    // this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand/?category="+catId;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = this.BrandData.length;
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  UserdData = [];
  NewUserdData = [];
  //  countUpdatedAt : Date;
  //  masterVehicleData= [];
  UserDataCount: any = [];
  getUserData() {
    // debugger
    this.spinnerService.show();
    let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          //myNewList =  Array.from(new Set(myList ));
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  getUserDataCountfilter(filterBody) {
    // debugger
    this.spinnerService.show();
    let localUrl = "user/?";

    // // let localUrl = "stat/ticket/?status=3&"
if (filterBody.state) {
localUrl = localUrl + "state=" +filterBody.state + "&"
}
if (filterBody.city) {
localUrl = localUrl + "city=" +filterBody.city + "&"
}
if (filterBody.fromDate && filterBody.toDate) {
localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
}
let url = this.server_url + localUrl;


    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          //myNewList =  Array.from(new Set(myList ));
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  getPanelMemberDatafilter(filterBody) {
    debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    // var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    // var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    // var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
    this.spinnerService.show();
    let localUrl = "user/?is_panel_member=1&is_logged_in=1&";

    if (filterBody.state) {
      localUrl = localUrl + "state=" +filterBody.state + "&"
      }
      if (filterBody.city) {
      localUrl = localUrl + "city=" +filterBody.city + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.PanelMemberData = res['results'];
          this.PanelMemberDataCount = res['count'];
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }


  getNonPanelMemberDatafilter(filterBody) {
    debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    // var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    // var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    // var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
    this.spinnerService.show();
    let localUrl = "user/?is_panel_member=0&is_logged_in=1&";

    if (filterBody.state) {
      localUrl = localUrl + "state=" +filterBody.state + "&"
      }
      if (filterBody.city) {
      localUrl = localUrl + "city=" +filterBody.city + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.NonPanelMemberData = res['results'];
          this.NonPanelMemberDataCount = res['count'];
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  getUserDataTodayscounter(filterBody) {
    debugger
    this.spinnerService.show();

    // var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    // var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime();
    // var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime();

    let localUrl = "user/?logged_in_today=1&";
    // let localUrl = "user/?from="+filterBody.fromDate+"&to="+filterBody.toDate;
    if (filterBody.state) {
      localUrl = localUrl + "state=" +filterBody.state + "&"
      }
      if (filterBody.city) {
      localUrl = localUrl + "city=" +filterBody.city + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+Math.round(filterBody.fromDate)+"&"+"to="+Math.round(filterBody.toDate);
      }
      let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodays = res['results'];
          //myNewList =  Array.from(new Set(myList ));
          // this.UserdDataTodays = res['results'];
          this.UserdDataTodaysCount = res['count'];
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  UserdDataTodaysAppUse;
  UserdDataTodaysAppUseCount;
  getUserDataTodaysAppUsedcounter(filterBody) {
    debugger
    this.spinnerService.show();

    // var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    // var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime();
    // var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime();

    let localUrl = "stat/invoice/user/?";
    // let url = this.server_url + localUrl;

    if (filterBody.state) {
      localUrl = localUrl + "state=" +filterBody.state + "&"
      }
      if (filterBody.city) {
      localUrl = localUrl + "city=" +filterBody.city + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodaysAppUse = res;
          //myNewList =  Array.from(new Set(myList ));
          // this.UserdDataTodays = res['results'];
          this.UserdDataTodaysAppUseKeys = Object.keys(this.UserdDataTodaysAppUse);
          var checkCount = [];
          for(let c =0;c<this.UserdDataTodaysAppUseKeys.length;c++){
            // for(let d =0;d<=this.UserdDataTodaysAppUse;d++){
              
              checkCount.push((this.UserdDataTodaysAppUse[this.UserdDataTodaysAppUseKeys[c]]).length)
           
          // }
        }
        this.UserdDataTodaysAppUseCount = checkCount.reduce((acc, cur) => acc + cur, 0);;
        
          
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  
  getRegististeredUserDataFilter(filterBody) {
    debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    // var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    // var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    // var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
   // var toDate= null;
   // var fromDate = null;
    this.spinnerService.show();
    let localUrl = "user/?is_logged_in=1&";
    // let url = this.server_url + localUrl;
    if (filterBody.state) {
      localUrl = localUrl + "state=" +filterBody.state + "&"
      }
      if (filterBody.city) {
      localUrl = localUrl + "city=" +filterBody.city + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.registerUserData = res['results'];
          this.registerUserDataCount = res['count'];
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  UserdDataTodaysAppUseKeys;
  getUserDataTodaysAppUsed() {
    debugger
    this.spinnerService.show();

    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;

    let localUrl = "stat/invoice/user/?from="+Math.round(fromDate)+"&to="+Math.round(toDate);
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodaysAppUse = res;
          //myNewList =  Array.from(new Set(myList ));
          // this.UserdDataTodays = res['results'];
          this.UserdDataTodaysAppUseKeys = Object.keys(this.UserdDataTodaysAppUse);
          var checkCount = [];
          for(let c =0;c<this.UserdDataTodaysAppUseKeys.length;c++){
            // for(let d =0;d<=this.UserdDataTodaysAppUse;d++){
              
              checkCount.push(this.UserdDataTodaysAppUse[this.UserdDataTodaysAppUseKeys[c]])
            
          // }
        }
        this.UserdDataTodaysAppUseCount = checkCount[0].length;
          
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }


  UserdDataTodays=[];
  UserdDataTodaysCount:any =[];

  getUserDataTodays() {
    debugger
    this.spinnerService.show();

    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;

    let localUrl = "user/?logged_in_today=1";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodays = res['results'];
          //myNewList =  Array.from(new Set(myList ));
          // this.UserdDataTodays = res['results'];
          this.UserdDataTodaysCount = res['count'];
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  ProductData;
   ProductDataCount;
   getProductData(brandId) {
      debugger
     //this.id = this.route.snapshot.paramMap.get('id');
     this.spinnerService.show();
     let localUrl = "product/?brand="+brandId;
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.ProductData = res['results'];
           this.ProductDataCount = res['count'];
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   PanelMemberData;
   PanelMemberDataCount:number;
   getPanelMemberData() {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
     var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
     var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
     var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
    // var toDate= null;
    // var fromDate = null;
     this.spinnerService.show();
     let localUrl = "user/?is_panel_member=1&is_logged_in=1";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.PanelMemberData = res['results'];
           this.PanelMemberDataCount = res['count'];
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   registerUserData;
   registerUserDataCount;
   getRegististeredUserData() {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
     var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
     var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
     var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
    // var toDate= null;
    // var fromDate = null;
     this.spinnerService.show();
     let localUrl = "user/?is_logged_in=1";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.registerUserData = res['results'];
           this.registerUserDataCount = res['count'];
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   downloadCode() {

    let localUrl = "user/code/?is_logged_in=1&";

    if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }
      

     let url = this.server_url + localUrl;

    window.open(url);
    
   }
   downloadCode2() {

    let localUrl = "user/code/?is_panel_member=1&is_logged_in=1&";

    if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }
     let url = this.server_url + localUrl;

    window.open(url);
    
   }

   downloadCode5() {

    let localUrl = "user/code/?is_panel_member=0&is_logged_in=1&";

    if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }
     let url = this.server_url + localUrl;

    window.open(url);
    
   }

   downloadCode3() {

    let localUrl = "user/code/?logged_in_today=1";
     let url = this.server_url + localUrl;

    window.open(url);
    
   }

   downloadCode4() {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;

    let localUrl = "stat/invoice/user/code/?from="+Math.round(fromDate)+"&to="+Math.round(toDate);

    // let localUrl = "user/code/?logged_in_today=1";
     let url = this.server_url + localUrl;

    window.open(url);
    
   }

   NonPanelMemberData;
   NonPanelMemberDataCount;
   getNonPanelMemberData() {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
     var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
     var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
     var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
    // var toDate= null;
    // var fromDate = null;
     this.spinnerService.show();
     let localUrl = "user/?is_panel_member=0&is_logged_in=1";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.NonPanelMemberData = res['results'];
           this.NonPanelMemberDataCount = res['count'];
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   StateData;
   StateDataCount;
   StateDataList;
  //  PanelMemberDataCount;
   getStateData() {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
//  var BATTUTA_KEY = "37cf8f32e2374f064d65ee8c8c9871f0";
     this.spinnerService.show();
     let localUrl = "user/state/";
     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {

          // this.StateDataCount = res['count'];
          // var xyz = res;
          // this.StateData = this.StateData.concat(res['results']);
          // if(this.StateData.length < this.StateDataCount) {
          //   this.getStateData(statepageNo +1);
          // }
           this.StateDataList = res;
          // //  this.PanelMemberDataCount = res['count'];
          // for(var s = 0; s<=this.StateData.length;s++ ){
          //   if(this.StateData[s].state != null){
          //     if(this.StateDataList.indexOf(this.StateData[s].state.toLowerCase()) == -1) {
          //       this.StateDataList.push(this.StateData[s].state.toLowerCase());
          //    }
          this.StateDataList.sort((a, b) => {
            if (a < b) return -1;
            else if (a > b) return 1;
            else return 0;
          });

          //   }
          // }
          
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   downloadExcel() {

    //   const type = 'application/vnd.ms-excel';
    //   const filename = 'invoice.xls';
    //   const options = new RequestOptions({
    //             responseType: ResponseContentType.Blob,
    //             headers: new Headers({ 'Accept': type })
    //         });
    
    //   this.http.get('http://10.2.2.109/Download/exportExcel', options)
    //            .catch(errorResponse => Observable.throw(errorResponse.json()))
    //            .map((response) => { 
    //                  if (response instanceof Response) {
    //                     return response.blob();
    //                  }
    //                  return response;
    //             })
    //            .subscribe(data => saveAs(data, filename),
    //                       error => console.log(error)); // implement your error handling here
    
    //
    if (this.downloadxcelFromDate && this.downloadxcelToDate) {
    window.open("http://mbill.market-xcel.com:8000/api/export/all/invoice/?from="+this.downloadxcelFromDate+"&to="+this.downloadxcelToDate);
    }
    else{
      window.open("http://mbill.market-xcel.com:8000/api/export/all/invoice/");
    }
   }

   

   cityData;
   cityDataCount;
   cityDataList=[];
  //  PanelMemberDataCount;
   getCityData(state) {
     debugger
     //this.id = this.route.snapshot.paramMap.get('id');
//  var BATTUTA_KEY = "37cf8f32e2374f064d65ee8c8c9871f0";
     this.spinnerService.show();
    //  let localUrl = "user/?limit=10000000";
    //  let url = this.server_url + localUrl;
     
     this.http.get("assets/cities.json",this.httpOptions)
       .subscribe(
         (res: Response) => {

          // this.StateDataCount = res['count'];
          // var xyz = res;
          // this.StateData = this.StateData.concat(res['results']);
          // if(this.StateData.length < this.StateDataCount) {
          //   this.getStateData(statepageNo +1);
          // }
           this.cityData = res;
           this.cityDataList = this.cityData.filter(
            city => city.state == state[0].toUpperCase() + state.slice(1));
            this.cityDataList.sort((a, b) => {
              if (a.name < b.name) return -1;
              else if (a.name > b.name) return 1;
              else return 0;
            });
          // //  this.PanelMemberDataCount = res['count'];
          // for(var s = 0; s<=this.cityData.length;s++ ){
          //   if(this.cityData[s].city != null){
          //     if(this.cityDataList.indexOf(this.cityData[s].city.toLowerCase()) == -1) {
          //       this.cityDataList.push(this.cityData[s].city.toLowerCase());
          //    }
              

          //   }
          // }
          
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
   }

   userpageNo: number = 1;
  pageChange(pageNo: any) {
    debugger
    this.userpageNo = pageNo;
    console.log("pageNo: " + this.userpageNo);
    this.spinnerService.show();

    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime();
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime();

    let localUrl = "user/?logged_in_today=1&page="+this.userpageNo;

  
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodays = res['results'];
          //myNewList =  Array.from(new Set(myList ));
          this.UserdDataTodaysCount = res['count'];
          // this.UserdDataTodaysCount = this.UserdDataTodays.length;
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    
    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
  }

  userpageNoo: number = 1;
  pageChange2(pageNo: any) {
    debugger
    this.userpageNoo = pageNo;
    console.log("pageNo: " + this.userpageNoo);

    this.spinnerService.show();
     let localUrl = "user/?is_panel_member=0&is_logged_in=1&page="+this.userpageNoo+"&";
     if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }

     let url = this.server_url + localUrl;
     
     this.http.get(url,this.httpOptions)
       .subscribe(
         (res: Response) => {
           this.NonPanelMemberData = res['results'];
           this.NonPanelMemberDataCount = res['count'];
           this.spinnerService.hide();
         },
         () => { alert("Error occoured"); this.spinnerService.hide(); }
       );
    
    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
  }

  userpageNo3: number = 1;
  pageChange3(pageNo: any) {
    debugger
    this.userpageNo3 = pageNo;
    console.log("pageNo: " + this.userpageNo3);

    this.spinnerService.show();
    let localUrl = "user/?is_panel_member=1&is_logged_in=1&page="+this.userpageNo3+"&";
    if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.PanelMemberData = res['results'];
          this.PanelMemberDataCount = res['count'];
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    
    //localStorage.setItem('currentPage', pageNo);
    //this.DashboardData(this.dashboardpageNo);
  }
  userpageNo4: number = 1;
  pageChange4(pageNo: any) {
    debugger
    this.userpageNo4 = pageNo;
    console.log("pageNo: " + this.userpageNo4);
    this.spinnerService.show();

    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;

    let localUrl = "stat/invoice/user/?from="+fromDate+"&to="+toDate+"&page="+this.userpageNo4;

    


    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdDataTodaysAppUse = res;
          this.UserdDataTodaysAppUseKeys = Object.keys(this.UserdDataTodaysAppUse);
          var checkCount = [];
          for(let c =0;c<this.UserdDataTodaysAppUseKeys.length;c++){
            // for(let d =0;d<=this.UserdDataTodaysAppUse;d++){
              
              checkCount.push(this.UserdDataTodaysAppUse[this.UserdDataTodaysAppUseKeys[c]])
            this.UserdDataTodaysAppUseCount = checkCount.length;
          // }
        }
          
          this.spinnerService.hide();
        
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  userpageNo5: number = 1;
  pageChange5(pageNo: any) {
    debugger
    this.userpageNo5 = pageNo;
    console.log("pageNo: " + this.userpageNo5);
    //this.id = this.route.snapshot.paramMap.get('id');
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    var fromDate = (new Date(new Date().setHours(0,0,0,0) - tzoffset)).getTime()/1000;
    var toDate = (new Date(new Date().setHours(23,59,59,999) - tzoffset)).getTime()/1000;
   // var toDate= null;
   // var fromDate = null;
    this.spinnerService.show();
    let localUrl = "user/?is_logged_in=1&page="+this.userpageNo5+"&";

    if (this.filterBodyForDownloadCode.state) {
      localUrl = localUrl + "state=" +this.filterBodyForDownloadCode.state + "&"
      }
      if (this.filterBodyForDownloadCode.city) {
      localUrl = localUrl + "city=" +this.filterBodyForDownloadCode.city + "&"
      }
      if (this.filterBodyForDownloadCode.fromDate && this.filterBodyForDownloadCode.toDate) {
      localUrl = localUrl +"from="+this.filterBodyForDownloadCode.fromDate+"&"+"to="+this.filterBodyForDownloadCode.toDate;
      }

    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.registerUserData = res['results'];
          this.registerUserDataCount = res['count'];
          this.spinnerService.hide();
        },
        () => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }
  
  ngOnInit() {
    $('body').addClass('page-header-fixed');
    this.getCategoryData();

    // this.getBrandData();
    this.getUserData();
    // this.getProductData();
    this.getPanelMemberData();
    this.getUserDataTodaysAppUsed();
    this.getRegististeredUserData();
    this.getNonPanelMemberData();
    // this.getBrandStatData();
    // this.getbargraph();
    // this.getShopPie();
    // this.LocationPie();
    // this.getCityData();
    this.getStateData();
    this.getUserDataTodays();
    this.barChartData=[];
    this.barChartLabels=[];
  } //ngOnInit close



  


} // class AdminDashboardComponent close

