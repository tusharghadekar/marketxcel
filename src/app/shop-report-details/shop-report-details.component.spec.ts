import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopReportDetailsComponent } from './shop-report-details.component';

describe('ShopReportDetailsComponent', () => {
  let component: ShopReportDetailsComponent;
  let fixture: ComponentFixture<ShopReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
