import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  private isUserLoggedIn;
  public _username;
  public username;
  private UserLoggedIn;

  constructor() { 
  	this.isUserLoggedIn = false;
  }

  setUserLoggedIn() {
    this.isUserLoggedIn = true;
//    this.username = username;
  }

  getUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  setUserLoggedInName(username:string) {
    this._username = username;
  }
  getUserLoggedInName() {
  	return this._username;
  }

}