import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
import { FormControl, FormGroup } from '@angular/forms';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-inventory-management',
  templateUrl: './inventory-management.component.html',
  styleUrls: ['./inventory-management.component.css']
})
export class InventoryManagementComponent implements OnInit {

  server_url : string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };

 public viewInventory:string;

  constructor(private _router: Router,private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService,private location: Location, private user: UserService) { }
id;
  InventoryData;
  inventoryDataCount;
  getInventoryData() {
    debugger
    this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "inventory?user="+this.id;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.InventoryData = res['results'];
          this.inventoryDataCount = res['count'];

          this.InventoryData.forEach(obj => {

            let localUrl = "user/"+obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.code=xyz["code"];
                  this.spinnerService.hide();
                },
                err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
          });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  onIsActiveChange(id:number, isActive:string){
    //
    if(confirm("Are you sure ?")){
      if(isActive == 'true')
      {
        isActive = 'false';
      }else if(isActive == 'false'){
        isActive = 'true';
      }
      let localUrl = "inventory/"+id;
      let url = this.server_url+localUrl;
  
      
      const deactiveInventoryObject = { is_active : isActive }
      let jsonInventoryBody = JSON.stringify(deactiveInventoryObject);
  
  this.http.patch(url, jsonInventoryBody, this.httpOptions).subscribe(
  (res) => {
    if(res)
  //this.categoryData = res['data'];
  alert("You have changed staus  successfully..");
  //this.viewopsVehicleSalesData = '';
  this.getInventoryData();
  });
  }else{alert("You made no change"); 
  // this.getopsVehicleAuditsMasterData(this.VehicleAuditspageNo); this.viewopsVehicleAudits = '';
  }
  }

  updateInventory(beforeUpdateInventoryObj) {
    debugger
      this.beforeUpdateInventoryObj = { 
        "name": beforeUpdateInventoryObj.name,
        "serial_no": beforeUpdateInventoryObj.serial_no,
        "user": beforeUpdateInventoryObj.user,
        "product": beforeUpdateInventoryObj.product,
        "quantity": beforeUpdateInventoryObj.quantity,
        "price": beforeUpdateInventoryObj.price,
        "is_active":beforeUpdateInventoryObj.is_active
     };
     let localUrl = "inventory/"+beforeUpdateInventoryObj.id+"/";
     let url = this.server_url+localUrl;
    
     this.http.patch(url, JSON.stringify(this.beforeUpdateInventoryObj), this.httpOptions).subscribe(
       (res) => {
         if(res){
          this.beforeUpdateInventoryObj = {
            "id": res['id'], 
           "serial_no": res['serial_no'],
           "user": res['user'],
           "product" : res['product'],
           "quantity" : res['quantity'],
           "price" : res['price'],
           "is_active": res['is_active']
          };
          alert("Successfully Updated");
        this.viewInventory =''; 
        this.getInventoryData();      
       }else{alert("error Occured")}},
           );
        
         
    } // Update Auditor functionality close
    beforeUpdateInventoryObj;
    // Editing Auditor function 
    editInventory(InventoryData){
      
             if(confirm("You want to edit Inventory ?")){
            debugger;
             this.viewInventory = 'editInventory';
             let localUrl = "inventory/"+InventoryData.id+"/";
             let url = this.server_url+localUrl;
             const checkinventoryObject = { id : InventoryData.id }
    
     this.http.patch(url,JSON.stringify(checkinventoryObject), this.httpOptions).subscribe(
     (res) => {
       if(res)
       this.beforeUpdateInventoryObj = {
         "id": res['id'], 
        "serial_no": res['serial_no'],
        "user": res['user'],
        "product" : res['product'],
        "quantity" : res['quantity'],
        "price" : res['price'],
        "is_active": res['is_active']
       };
    
           }, err =>{alert("You made no change");}
         );
       }else{}
      
     }

     uploadForm = new FormGroup ({
      //file1: new FormControl()
    });
    filedata:any;
    //imageUrl : string ="/importedFiles/targetdataBaseFiles";
    
    
    fileChange(e){
        console.log(e);
    debugger;
    //let Key = e.target.elements[1].value;
    //let fileList: FileList = e.target.files;
        this.filedata = e.target.files[0];
    }
    
    onSubmit(e) {
      // let currentUserRoleid = JSON.parse( localStorage.getItem('userRoleid'));
      //   if(currentUserRoleid == "2"){
      //     alert("You Are Not Authenticate to Upload Sales Data")
      //   }else{
      this.spinnerService.show();
      e.preventDefault();
      debugger;
    
      let formData:FormData = new FormData();
      let file: File = this.filedata;
      //let Key = FileList[0];
      //console.log(this.uploadForm);
      formData.append("image",file);
      let httpOptions= {
        headers: new HttpHeaders({ 
          //'content-type':'multipart/form-data',
          'Authorization': 'JWT ' + this.tokens
        })
      };
      let localUrl = "import";
      let url = this.server_url+localUrl;
      this.http.post(url,formData,httpOptions)
      .subscribe((res)=>{
        console.log("res"+res);
        if(res['status'] == true) {
          alert("Your "+file.name+"  uploaded successfully..");
          this.spinnerService.hide();
          this.viewInventory ='';
          this.getInventoryData();
           }else if(res['status'] == false) {
            alert("File NOT Imported");
            this.spinnerService.hide();
          }else{
            alert("Server Not working, Please Try after some time or contact to Administrator");
            this.spinnerService.hide();
          }
           
      },
      // error => {
      //   if((error['error']['status'] == false) &&  (error['error']['messageCode'] == "QIZ021")) {
      //     alert( "City,Vehicle Versions,Transmission Type and Colour Can Not Be Blank or Wrong"+" "+JSON.stringify(error['error']['data']));this.spinnerService.hide();
      //   } else if((error['error']['status'] == false) && (error['error']['error'])) {
      //     alert( "vin must be unique."+" "+JSON.stringify(error['error']['messageCode']['fields']));this.spinnerService.hide();
      // }
      // else{alert( "Failed to Import.");this.spinnerService.hide(); }
      // }
    );
    
    // }
    }

    InventorypageNo: number = 1;
  pageChange(pageNo: any) {
    //debugger
    this.InventorypageNo = pageNo;
    console.log("pageNo: " + this.InventorypageNo);
    let id = this.route.snapshot.paramMap.get('id');
    debugger
    this.spinnerService.show();
    let localUrl = "inventory/?user="+id+"&page="+this.InventorypageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.InventoryData = res['results'];
          this.inventoryDataCount = res['count'];

          this.InventoryData.forEach(obj => {

            let localUrl = "user/"+obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
            
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.code=xyz["code"];
                  this.spinnerService.hide();
                },
                err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            
          });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
        
  }

  Back() {
    //this.viewShops = 'addShops';
    this.location.back();
  }

  ngOnInit() {
    this.getInventoryData();
  }

}
