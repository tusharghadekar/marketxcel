import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response,ResponseContentType } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
import { InvokeFunctionExpr } from '../../../node_modules/@angular/compiler';
declare var jquery: any;
declare var $: any;
import 'rxjs/Rx' ;

@Component({
  selector: 'app-shop-report',
  templateUrl: './shop-report.component.html',
  styleUrls: ['./shop-report.component.css']
})
export class ShopReportComponent implements OnInit {

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService, private location: Location, private user: UserService) { }

  id: any;
  ShopReportData;
  ShopReportDataCount;
  ShopReportDataPurchase;
  getShopReportData() {
    debugger
    this.id = this.route.snapshot.paramMap.get('id');
    localStorage.setItem('vendorid', this.id);
    this.spinnerService.show();
    let localUrl = "invoice?user=" + this.id;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ShopReportData = res['results'];
          this.ShopReportDataCount = res['count'];
          //  this.ShopReportDataPurchase = this.ShopReportData.map(results=>results.purchase);
          //  this.ShopReportDataPurchase = this.ShopReportDataPurchase.flatMap(result=>result);

          //    this.ShopReportDataPurchase.forEach(obj => {

          //     let localUrl = "product/"+obj.product;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.name=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          //   this.ShopReportDataPurchase.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          //   this.ShopReportDataPurchase.forEach(obj => {

          //     let localUrl = "brand/"+obj.brand;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.brandname=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          this.ShopReportData.forEach(obj => {

            let localUrl = "user/" + obj.user;
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.code = xyz["code"];
                  this.spinnerService.hide();
                },
                err => { alert("Error occoured"); this.spinnerService.hide(); }
              );

          });

          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  //  id;  
  //  ProductData;
  //  ProductDataCount;
  //  getProductData() {
  //    //debugger
  //   //  this.id = this.route.snapshot.paramMap.get('id');
  //    this.spinnerService.show();
  //    let localUrl = "product/";
  //    //let localUrl = "brand/";
  //    let url = this.server_url + localUrl;

  //    this.http.get(url,this.httpOptions)
  //      .subscribe(
  //        (res: Response) => {
  //          this.ProductData = res['results'];
  //          this.ProductDataCount = res['count'];
  //          this.spinnerService.hide();
  //        },
  //        err => { alert("Error occoured"); this.spinnerService.hide(); }
  //      );
  //  }
  // prodname;
  // prodid;
  //  getproduct(id) {
  //   //debugger
  //       for(let i=0; i<this.ProductData.length; i++){
  //         if(this.ProductData[i].id === id){
  //           this.prodname = this.ProductData[i].name;
  //           return this.prodname;
  //           //console.log("catname: " + this.catname[i]);
  //         }
  //       }
  //     }

  //     dealercode;
  //  getdealercode(id) {
  //   //debugger
  //       for(let i=0; i<this.UserdData.length; i++){
  //         if(this.UserdData[i].id === id){
  //           this.dealercode = this.UserdData[i].code;
  //           return this.dealercode;
  //           //console.log("catname: " + this.catname[i]);
  //         }
  //       }
  //     }

  UserdData = [];
  UserDataCount: any = [];
  getUserData() {
    //debugger
    this.spinnerService.show();
    let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  reportpageNo: number = 1;
  pageChange(pageNo: any) {
    //debugger
    this.reportpageNo = pageNo;
    console.log("pageNo: " + this.reportpageNo);

    debugger
    this.id = this.route.snapshot.paramMap.get('id');
    
    this.spinnerService.show();
    let localUrl = "invoice/?page=" + this.reportpageNo + "&user=" + this.id;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ShopReportData = res['results'];
          this.ShopReportDataCount = res['count'];
          // this.ShopReportDataPurchase = this.ShopReportData.map(results=>results.purchase);
          // this.ShopReportDataPurchase = this.ShopReportDataPurchase.flatMap(result=>result);

          //    this.ShopReportDataPurchase.foreach(obj => {
          //     obj.foreach(childObj=> {
          //       //value.checked = parentChecked;
          //    })
          // });

          //    this.ShopReportDataPurchase.forEach(obj => {

          //     let localUrl = "product/"+obj.product;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.name=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          //   this.ShopReportDataPurchase.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          //   this.ShopReportData.forEach(obj => {

          //     let localUrl = "brand/"+obj.brand;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.brandname=xyz["name"];
          //           this.spinnerService.hide();
          //         },
          //         err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });

          this.ShopReportData.forEach(obj => {

            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.code = xyz["code"];
                  this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );

          });
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  catname;
  getcategoryName(id) {
    //debugger
    for (let i = 0; i < this.categoryData.length; i++) {
      if (this.categoryData[i].id === id) {
        this.catname = this.categoryData[i].name;
        return this.catname;
        //console.log("catname: " + this.catname[i]);
      }
    }
  }
  brandname;
  getbrandName(id) {
    //debugger
    for (let i = 0; i < this.BrandData.length; i++) {
      if (this.BrandData[i].id === id) {
        this.brandname = this.BrandData[i].name;
        return this.brandname;
        //console.log("catname: " + this.catname[i]);
      }
    }
  }

  BrandData;
  brandDataCount;
  getBrandData() {
    //debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  categoryData = [];
  catDataCount;
  getCategoryData() {
    debugger
    this.spinnerService.show();
    let localUrl = "category/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  back() {
    this.location.back();

  }

  viewreportdetails(id) {
    this._router.navigate(['shopreportdetails/' + id]);
  }

  DownloadReport() {
    this.spinnerService.show();
    debugger
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'JWT '+ this.tokens
      })
    };
    // let FilterBodyForLink = this.ReportService.getFilterData();
    let localUrl = "export/invoice/";
    let url = this.server_url + localUrl;
    this.http.get(url, httpOptions)
      .subscribe(
        (res: Response) => {

          var blob = new Blob([res], { type: 'text/csv'});
          var objectUrl = window.URL.createObjectURL(blob);
          //window.open(objectUrl);
          window.open(objectUrl);

          this.spinnerService.hide();
          // window.open(url);

        },
        // err => { 
        //   alert(err)
        //   alert("Error occoured"); this.spinnerService.hide(); 
        // }
      );

    // $.ajax({  
    //   url: 'http://34.218.151.6:9000/api/export/invoice/',  
    //   type: 'GET',  
    //   //dataType: 'json',
    //   headers: {"Authorization": 'JWT '+this.tokens},  
    //   success: function (data, textStatus, xhr) {  
    //     window.open();
    //       console.log(data);
    //       this.spinnerService.hide();  
    //   },  
    //   error: function (xhr, textStatus, errorThrown) {  
    //       console.log('Error in Operation'); 
    //       this.spinnerService.hide(); 
    //   }
    // });this.spinnerService.hide();
  }


  downloadExcel() {

  //   const type = 'application/vnd.ms-excel';
  //   const filename = 'invoice.xls';
  //   const options = new RequestOptions({
  //             responseType: ResponseContentType.Blob,
  //             headers: new Headers({ 'Accept': type })
  //         });
  
  //   this.http.get('http://10.2.2.109/Download/exportExcel', options)
  //            .catch(errorResponse => Observable.throw(errorResponse.json()))
  //            .map((response) => { 
  //                  if (response instanceof Response) {
  //                     return response.blob();
  //                  }
  //                  return response;
  //             })
  //            .subscribe(data => saveAs(data, filename),
  //                       error => console.log(error)); // implement your error handling here
  
  //
  
  window.open("http://mbill.market-xcel.com:8000/api/export/invoice?user="+this.id)
 }
 


  
  ngOnInit() {
    this.getShopReportData();
    this.getBrandData();
    this.getCategoryData();
  }

}
