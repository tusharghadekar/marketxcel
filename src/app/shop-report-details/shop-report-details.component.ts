import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DateService } from '../../app/services/date/date.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserService } from '../user.service';
import { AuthService } from '../interceptors/auth.service';
import { IMyDrpOptions } from 'mydaterangepicker';
import { ReportService } from '../services/report/report.service';
import { DatepickerOptions } from 'ng2-datepicker';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import * as enLocale from 'date-fns/locale/en';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-shop-report-details',
  templateUrl: './shop-report-details.component.html',
  styleUrls: ['./shop-report-details.component.css']
})
export class ShopReportDetailsComponent implements OnInit {

  server_url : string = environment.devServer_url;
  tokens:any = localStorage.getItem('token');
 public httpOptions = {
   headers: new HttpHeaders({
     'Content-Type': 'application/json',
    'Authorization': 'JWT '+this.tokens
   })
 };

 constructor(private _router: Router,private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService, private http: HttpClient, private _dateservice: DateService,private location: Location, private user: UserService) { }

 id:any;
 ShopReportData;
 ShopReportDataCount;
 ShopReportDataPurchase;
 getShopReportDetailsData() {
   debugger
   this.id = this.route.snapshot.paramMap.get('id');
   this.spinnerService.show();
   let localUrl = "invoice/"+this.id;
   //let localUrl = "brand/";
   let url = this.server_url + localUrl;
   
   this.http.get(url,this.httpOptions)
     .subscribe(
       (res: Response) => {
         this.ShopReportDataPurchase = res['purchase'];
         this.ShopReportDataCount = this.ShopReportDataPurchase.length;
        //  this.ShopReportDataPurchase = this.ShopReportData.map(results=>results.purchase);
        //  this.ShopReportDataPurchase = this.ShopReportDataPurchase.flatMap(result=>result);

      //    this.ShopReportDataPurchase.forEach(obj => {

      //     let localUrl = "product/"+obj.product;
      //     //let localUrl = "brand/";
      //     let url = this.server_url + localUrl;
          
      //     this.http.get(url,this.httpOptions)
      //       .subscribe(
      //         (res: Response) => {
      //           var xyz=res;
      //           obj.name=xyz["name"];
      //           this.spinnerService.hide();
      //         },
      //         err => { alert("Error occoured"); this.spinnerService.hide(); }
      //       );
          
      // });

    //   this.ShopReportDataPurchase.forEach(obj => {

    //     let localUrl = "category/"+obj.category;
    //     //let localUrl = "brand/";
    //     let url = this.server_url + localUrl;
        
    //     this.http.get(url,this.httpOptions)
    //       .subscribe(
    //         (res: Response) => {
    //           var xyz=res;
    //           obj.catname=xyz["name"];
    //           this.spinnerService.hide();
    //         },
    //         err => { alert("Error occoured"); this.spinnerService.hide(); }
    //       );
        
    // });

  //   this.ShopReportDataPurchase.forEach(obj => {

  //     let localUrl = "brand/"+obj.brand;
  //     //let localUrl = "brand/";
  //     let url = this.server_url + localUrl;
      
  //     this.http.get(url,this.httpOptions)
  //       .subscribe(
  //         (res: Response) => {
  //           var xyz=res;
  //           obj.brandname=xyz["name"];
  //           this.spinnerService.hide();
  //         },
  //         err => { alert("Error occoured"); this.spinnerService.hide(); }
  //       );
      
  // });

  this.ShopReportDataPurchase.forEach(obj => {

    let localUrl = "user/"+obj.user;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          var xyz=res;
          obj.code=xyz["code"];
          this.spinnerService.hide();
        },
        //err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
    
});

         this.spinnerService.hide();
       },
       err => { alert("Error occoured"); this.spinnerService.hide(); }
     );
 }

//  id;  
 ProductData;
 ProductDataCount;
 getProductData() {
   //debugger
  //  this.id = this.route.snapshot.paramMap.get('id');
   this.spinnerService.show();
   let localUrl = "product/";
   //let localUrl = "brand/";
   let url = this.server_url + localUrl;
   
   this.http.get(url,this.httpOptions)
     .subscribe(
       (res: Response) => {
         this.ProductData = res['results'];
         this.ProductDataCount = res['count'];
         this.spinnerService.hide();
       },
       err => { alert("Error occoured"); this.spinnerService.hide(); }
     );
 }
prodname;
prodid;
 getproduct(id) {
  //debugger
      for(let i=0; i<this.ProductData.length; i++){
        if(this.ProductData[i].id === id){
          this.prodname = this.ProductData[i].name;
          return this.prodname;
          //console.log("catname: " + this.catname[i]);
        }
      }
    }

    dealercode;
 getdealercode(id) {
  //debugger
      for(let i=0; i<this.UserdData.length; i++){
        if(this.UserdData[i].id === id){
          this.dealercode = this.UserdData[i].code;
          return this.dealercode;
          //console.log("catname: " + this.catname[i]);
        }
      }
    }

    UserdData = [];
  UserDataCount: any = [];
  getUserData() {
    //debugger
    this.spinnerService.show();
    let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          this.spinnerService.hide();

        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

    reportpageNo: number = 1;
  pageChange(pageNo: any) {
    //debugger
    this.reportpageNo = pageNo;
    console.log("pageNo: " + this.reportpageNo);

    debugger
    let id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "invoice/"+id;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ShopReportDataPurchase = res['purchase'];
          this.ShopReportDataCount = this.ShopReportDataPurchase.length;
          // this.ShopReportDataPurchase = this.ShopReportData.map(results=>results.purchase);
          // this.ShopReportDataPurchase = this.ShopReportDataPurchase.flatMap(result=>result);
 
       //    this.ShopReportDataPurchase.foreach(obj => {
       //     obj.foreach(childObj=> {
       //       //value.checked = parentChecked;
       //    })
       // });

    //    this.ShopReportDataPurchase.forEach(obj => {

    //     let localUrl = "product/"+obj.product;
    //     //let localUrl = "brand/";
    //     let url = this.server_url + localUrl;
        
    //     this.http.get(url,this.httpOptions)
    //       .subscribe(
    //         (res: Response) => {
    //           var xyz=res;
    //           obj.name=xyz["name"];
    //           this.spinnerService.hide();
    //         },
    //         err => { alert("Error occoured"); this.spinnerService.hide(); }
    //       );
        
    // });

  //   this.ShopReportDataPurchase.forEach(obj => {

  //     let localUrl = "category/"+obj.category;
  //     //let localUrl = "brand/";
  //     let url = this.server_url + localUrl;
      
  //     this.http.get(url,this.httpOptions)
  //       .subscribe(
  //         (res: Response) => {
  //           var xyz=res;
  //           obj.catname=xyz["name"];
  //           this.spinnerService.hide();
  //         },
  //         err => { alert("Error occoured"); this.spinnerService.hide(); }
  //       );
      
  // });

//   this.ShopReportData.forEach(obj => {

//     let localUrl = "brand/"+obj.brand;
//     //let localUrl = "brand/";
//     let url = this.server_url + localUrl;
    
//     this.http.get(url,this.httpOptions)
//       .subscribe(
//         (res: Response) => {
//           var xyz=res;
//           obj.brandname=xyz["name"];
//           this.spinnerService.hide();
//         },
//         err => { alert("Error occoured"); this.spinnerService.hide(); }
//       );
    
// });

this.ShopReportDataPurchase.forEach(obj => {

  let localUrl = "user/"+obj.user;
  //let localUrl = "brand/";
  let url = this.server_url + localUrl;
  
  this.http.get(url,this.httpOptions)
    .subscribe(
      (res: Response) => {
        var xyz=res;
        obj.code=xyz["code"];
        this.spinnerService.hide();
      },
      //err => { alert("Error occoured"); this.spinnerService.hide(); }
    );
  
});
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  catname;
  getcategoryName(id) {
    //debugger
        for(let i=0; i<this.categoryData.length; i++){
          if(this.categoryData[i].id === id){
            this.catname = this.categoryData[i].name;
            return this.catname;
            //console.log("catname: " + this.catname[i]);
          }
        }
      }
      brandname;
      getbrandName(id) {
        //debugger
            for(let i=0; i<this.BrandData.length; i++){
              if(this.BrandData[i].id === id){
                this.brandname = this.BrandData[i].name;
                return this.brandname;
                //console.log("catname: " + this.catname[i]);
              }
            }
          }

  BrandData;
  brandDataCount;
  getBrandData() {
    //debugger
    //this.id = this.route.snapshot.paramMap.get('id');
    this.spinnerService.show();
    let localUrl = "brand";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.BrandData = res['results'];
          this.brandDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  categoryData = [];
catDataCount;
  getCategoryData() {
    debugger
    this.spinnerService.show();
    let localUrl = "category/";
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.categoryData = res['results'];
          this.catDataCount = res['count'];
          this.spinnerService.hide();
        },
        err => { alert("Error occoured"); this.spinnerService.hide(); }
      );
  }

  back(){
    this.location.back();

  }

  downloadExcel(){
    let vendorid = localStorage.getItem('vendorid');
    window.open("http://mbill.market-xcel.com:8000/api/export/invoice?user="+vendorid)
  }

  ngOnInit() {
    this.getShopReportDetailsData();
    this.getProductData();
    this.getBrandData();
    this.getCategoryData();
  }

}
