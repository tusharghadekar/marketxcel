import { Injectable } from '@angular/core';


@Injectable()
export class auditorsprofileService {

    _RoleId:number;
    _currentAuditorId:number;

    public setAuditorsprofileUserRole(RoleId:number){
        this._RoleId = RoleId;
    }
    public getAuditorsprofileUserRole(){
        return this._RoleId;
    }

    
    public setcurrentAuditor(currentAuditorId:number){
        this._currentAuditorId = currentAuditorId;
    }
    public getcurrentAuditor(){
        return this._currentAuditorId;
    }

}