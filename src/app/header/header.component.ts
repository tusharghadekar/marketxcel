import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { DateService } from '../../app/services/date/date.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public todayFullDate:Date;
  public userName:string;
  public isLogin:string;
  constructor(private _dateservice: DateService, private router:Router,private user:UserService,private http: HttpClient,) {
  this.userName = localStorage.getItem('currentUser');
    
    if (this.userName == null){
      this.router.navigate(['login-form']);
    }else{}
   }

   server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public beforeUpdateUserObj;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

   UserdData;
  UserDataCount: any = [];
  image;
  getUserDataa() {
    debugger
    let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserdData = res['results'];
          this.UserDataCount = res['count'];
          for(let i=0; i<this.UserDataCount;i++){
         if(this.UserdData[i].username == this.userName){
           //this.image = this.UserdData[i].image
           this.image = "../assets/layouts/layout2/img/user_icon.jpg";
         }
        }
        if(this.image == null){this.image = "../assets/layouts/layout2/img/user_icon.jpg";}
        },
        err => { alert("Error occoured"); }
      );
  }

  ngOnInit() {
    this.todayFullDate=  this._dateservice.getCurrentDate();
    //this.userName=  this.user.getUserLoggedInName();
    //this.isLogin=  this.user.getUserLoggedIn();
    //console.log("this.isLogin: "+this.isLogin)
    console.log("this.userNameheader: "+this.userName)
    this.getUserDataa();
  }

}
;